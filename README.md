<!-- https://i.pinimg.com/originals/5f/6b/71/5f6b712b179813da05f9523fcc73333b.jpg -->
[![](snakerain.png)](http://www.soul-guidance.com/houseofthesun/ET/rainsnakes.html)

# Rain on Python

Work in Progress: [ICON Report](https://sharelatex.gwdg.de/project/60e573919f8cc90090770cd0)

This project explores the possibilities of invoking python modules from Fortran code. The test subject will be the Kinematic Rain Model. Its Original in Fortran is part of the ![ICON source code](https://gitlab.dkrz.de/icon/icon/-/tree/icon-2.6.3-rc). The module has been re-written by David Leutwyler in Python (![github](https://github.com/leuty/Kinematic_Rain_Model)). Having a Python replacement for a Fortran module will facilitate exchanging code on a small scale and produce results that are comparable.
Previous notes on the subject of interfacing Python with Fortran can be found in this [gwdg pad](https://pad.gwdg.de/BkMeGPkpTz6hnsNKo9xN0w#).

## Possible interface solutions

- [**forpy**](https://github.com/ylikx/forpy), uses C bindings and shared C types. Is contained in a single `.f90` file. This brings limitations in terms of user friendlyness. It does everything, that is needed. Nice documentation. Passing arrays as pointers causes problems with compiler optimization in older compilers (ICON relies on gfortran 6.5).
- [**call_py_fort**](https://github.com/nbren12/call_py_fort), build on top of _CFFI_ (C Foreign Function Interface for Python), a library whose purpose it is to embed C and Fortran into Python (the other way round). The author has written a [blog post](https://www.noahbrenowitz.com/post/calling-fortran-from-python/) about it.
- [**Forthon**](https://github.com/dpgrote/Forthon), Python wrapper generator for Fortran. No documentation, just examples.
- [**forcallpy**](https://forcallpy.readthedocs.io/en/latest/#download), lets you embed a Python interpreter within your Fortran program, allow to call Python code and manage Fortran/Python data exchange. All this with secure type checking and less possible data duplication. Is on an unusual french academia git and the code is commented in english and french (mosty english though).


Further there are:
- [**pyccel**](https://github.com/pyccel/pyccel) that translates Python code into Fortran. It requires [static typing for the python code](https://www.section.io/engineering-education/python-static-typing/). This would be no problem for our application. But as most python libraries do not meet this requirement, only those that have been adjusted by the pyccel team are available. The numpy library for example is only partially available (as of 31.05.2021).
- [**fython**](https://fython.readthedocs.io/en/latest/), Fortran with a Python syntax. This is really just cosmetics. It adds no new features.


<!--  old list, containing fortran in python solutions
The [Fortran  wiki](http://fortranwiki.org/fortran/show/Python) mentions several options to be considered. These include:
- ![_forpy_](https://github.com/ylikx/forpy), use Python in Fortran and vice versa. Is contained in a single `.f90` file. This brings limitations. Nice documentation.
- ![_Forthon_](https://github.com/dpgrote/Forthon), Python wrapper generator for Fortran. No documentation, just examples.
- [_call_py_fort_](https://github.com/nbren12/call_py_fort), build on top of _CFFI_ (C Foreign Function Interface for Python), a library whose purpose it is to embed C and Fortran into Python (the other way round). The author has written a [blog post](https://www.noahbrenowitz.com/post/calling-fortran-from-python/) about it.
- [_F2x_](https://github.com/DLR-SC/F2x), a template-based Fortran wrapper written in Python. compares itself to _f2py_ and might not be apt for embedding python in fortran.
- [_f90wrap_](https://github.com/jameskermode/f90wrap), a tool to automatically generate Python extension modules which interface to Fortran code that makes use of derived types.on the SciPy wiki. It builds on the capabilities of the popular f2py utility by generating a simpler Fortran 90 interface to the original Fortran code which is then suitable for wrapping with f2py, together with a higher-level Pythonic wrapper that makes the existance of an additional layer transparent to the final user.
- [_forcallpy_](https://forcallpy.readthedocs.io/en/latest/#download), lets you embed a Python interpreter within your Fortran program, allow to call Python code and manage Fortran/Python data exchange. All this with secure type checking and less possible data duplication. Is on an unusual french academia git.
- [_fython_](https://fython.readthedocs.io/en/latest/), Fortran with a Python syntax. Dont know if thats any good.
- [_gfort2py_](https://github.com/rjfarmer/gfort2py), is probably just for embedding fortran in python. Uses gfortran `.mod` files to translate your fortran code’s ABI to python compatible types using python’s ctype library.
- [_f2py_](https://numpy.org/doc/stable/f2py/), a Fortran to Python interface generator. It is part of the latest numpy releases. Can be used to embed Fortran in Python. Hard to say if it can work the other way round.

Further there is:
- ![_pyccel_](https://github.com/pyccel/pyccel) could be used to translate python into fortran
-->

## Kinematic Rain module

As a starting point it has been decided to embed the Accrection function of the Kinematic Rain Model in Python into ICON, while keeping the rest of the Fortran module. 
- ![Python function in the Python module](https://github.com/leuty/Kinematic_Rain_Model/blob/595472c821b788ca16a4b0ebb6444e1b20ab66ed/kinematic_rain_model/two_moment_microphysics.py#L195)
- ![Fortran subroutine in the ICON code](https://gitlab.dkrz.de/icon/icon/-/blob/icon-2.6.3-rc/src/atm_phy_schemes/mo_2mom_mcrph_processes.f90#L859)


## Forpy

![Forpy](https://github.com/ylikx/forpy) provides data structures and the means of passing them both from Fortran to Python and the other way around. The supported data types include `list`, `dict`, `tuple` and `numpy.ndarray`. Python modules can be imported from Fortran and functions called. Forpy uses ![fypp](https://github.com/aradi/fypp), a python preprocessor for Fortran. 

- ![Report](https://gitlab.dkrz.de/m300872/rain-on-python/-/blob/master/forpy/report.md)
- ![Example code](https://gitlab.dkrz.de/m300872/rain-on-python/-/tree/master/forpy/example), consisting of a Fortran and Python code and a make file


## Test case 

A proper test case is needed. The test case should:
- be a minimal setup, with little dependencies. e.g. atmosphere only
- have a small computational load 
  - short model time span
  - low resolution
- use the two moment two moment microphysics (!)

I am woking on this. See [usage log](test_case/usage_log.md).

## Pending Issues / TODO

- **Deeper understanding:** It would be nice to have a deeper understanding of how _forpy_ and other solutions work. They all rely on C bindings. But this does not necessarily mean, that they are equal in mechanism and performance.
- **Git:** I have been shamefully neglecting Git in my development process. To have a working test case, that applies two moment microphysics, I am currently working on a specific commit of a specifc branch of the AES icon repository. I have uploaded and updated the crucial files to this repository, but i may be preferable to have a python-injection brach of icon. 
- **asynchronous arrays:** The current version of the python injection uses _forpy_ with the stable but slow approach to copy the arrays at each communication between Fortran and Python. This was chosen, because handing arrays as pointers would require to either turn off compiler optimization or to use the array keyword `asynchronous`, that is not implemented in the gfortran version that I am using (6.5). In a way this was a shortcut to have a first working version, before trying to optimize it. During the CIMD meeting it was suggested to try the use of these keyword with the Intel compiler and Luis claimed, that his ICON on the Julich mashine is compiled with gfortran 10 and working well.
- **Other solutions:** The initial list of possible interface solutions did shrink singnificantlty after I took a closer look. Only a handful solution exist for the rather exotic case to use Python from within Fortran (and not the other way round). All of those solutions use C bindings. This makes sense, as this feature is already implemented and promises good performance. But it does raise the question whether there is any difference at all between those solutions (See first point). A promising candidate is _call_py_fort_/_CFFI_, which apparently compiles Python code into a C library, that is then called from within Fortran.
- **Final report:** This repository has grown in the process and has become a bit confusing with its nested Readmes. A final report with a clearer structure should be made.

## Further Ideas for speed up of Python code

Apart from CPython, which is the standard way of running Python, there are other options. [PyPy](https://blog.paperspace.com/getting-started-with-pypy/) is one of these options. By compiling only needed code instead of all the code (JIT vs. AOT), it is singnificantlty faster than CPython in most applications. This may not apply to our use case, as PyPys spead up is much weaker when the executed code is not pure Python (e.g. numpy). It is [available for RHEL](https://snapcraft.io/install/pypy3/rhel).

Another source of slow down, that we have not considered seperately so far is import of modules in the Python program. Module import might take up to several 100ms. A workaround might be the use of a server-client structure (using the `socket` module). The time that is consumed by module imports can be profiled using a tool called `tuna`. Both is described in [this thread on stack overflow](https://stackoverflow.com/questions/16373510/improving-speed-of-python-module-import). 
