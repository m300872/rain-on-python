### TODO

### forpy

- [ ] separate the files and upload to directory
- [ ] make a script, that manipulates the icon source, injecting the python code
  - [ ] wget `2mo_mcrph_processes.f90`
  - [ ] wget `forpy.f90`
  - [ ] wget `two_moment_microphysics.py`
  - [ ] manipulate config script (compiler flags, export LD_LIBRARY_PATH, export PYTHON, module load python)


config script modifications:

```bash
MODULES='gcc/6.4.0 openmpi/2.0.2p1_hpcx-gcc64 python/3.5.2'
PYTHON="$(python3-config --prefix)/bin/python"
LDFLAGS="-L${HDF5_ROOT}/lib -L${NETCDF_ROOT}/lib -L${NETCDFF_ROOT}/lib -L${GRIBAPI_ROOT}/lib -L${MKL_ROOT}/lib/intel64 $(python3-config --ldflags) -Wl,-rpath -Wl,$(python3-config --prefix)/lib"
BUILD_ENV=". /sw/rhel6-x64/etc/profile.mistral; . \"${SCRIPT_DIR}/module_switcher\"; switch_for_module ${MODULES}; export LD_LIBRARY_PATH=\"${HDF5_ROOT}/lib:${NETCDF_ROOT}/lib:${NETCDFF_ROOT}/lib:${GRIBAPI_ROOT}/lib:${MKL_ROOT}/lib/intel64:\${python3-config --prefix}/lib:\${LD_LIBRARY_PATH}\";"
```

Well it does not.
Lets try on another branch






```bash
#!/bin/bash




# get the forpy specific files
target_dir="src/atm_phy_schemes/"
#base_url="https://gitlab.dkrz.de/m300872/rain-on-python/-/blob/master/forpy/single_routine_injection/"
base_url="https://gitlab.dkrz.de/m300872/rain-on-python/-/raw/master/forpy/single_routine_injection/"
file_list=(
        "mo_2mom_mcrph_processes.f90"
        "two_moment_microphysics.py"
        "forpy_mod.f90"
)
# loop over list and wget them
for file_name in "${file_list[@]}"; do
        echo "getting: $value"
        rm $target_dir/$file_name
        wget $base_url/$file_name -P $target_dir
done

# get alternative config script
target_dir="config/dkrz/"
file_name="mistral.forpy.gcc"
wget $base_url/$file_name -P $target_dir
chmod +x $target_dir/$file_name
# modify config/buildbot/mistral_gcc
file_name="config/buildbot/mistral_gcc" 
sed -i 's/mistral.gcc/mistral.forpy.gcc/' $file_name
```






```bash
MODULES='gcc/6.4.0 openmpi/2.0.2p1_hpcx-gcc64 python/3.5.2'
PYTHON="$(python3-config --prefix)/bin/python"
LDFLAGS="-L${HDF5_ROOT}/lib -L${NETCDF_ROOT}/lib -L${NETCDFF_ROOT}/lib -L${GRIBAPI_ROOT}/lib -L${MKL_ROOT}/lib/intel64 $(python3-config --ldflags) -Wl,-rpath -Wl,$(python3-config --prefix)/lib"
BUILD_ENV=". /sw/rhel6-x64/etc/profile.mistral; . \"${SCRIPT_DIR}/module_switcher\"; switch_for_module ${MODULES}; export LD_LIBRARY_PATH=\"${HDF5_ROOT}/lib:${NETCDF_ROOT}/lib:${NETCDFF_ROOT}/lib:${GRIBAPI_ROOT}/lib:${MKL_ROOT}/lib/intel64:\${python3-config --prefix}/lib:\${LD_LIBRARY_PATH}\";"
```


This script should do all the necessary.
```bash
#!/bin/bash


base_directory_name="icon_forpy_test"
# get the repo
git clone --recursive git@gitlab.dkrz.de:icon/icon.git $base_directory_name
cd $base_directory_name


# get the files
target_dir="src/atm_phy_schemes/"
#base_url="https://gitlab.dkrz.de/m300872/rain-on-python/-/blob/master/forpy/single_routine_injection/"
base_url="https://gitlab.dkrz.de/m300872/rain-on-python/-/raw/master/forpy/single_routine_injection/"
file_list=(
        "mo_2mom_mcrph_processes.f90"
        "two_moment_microphysics.py"
        "forpy_mod.f90"
)
# loop over list and wget them
for file_name in "${file_list[@]}"; do
        echo "getting: $value"
        rm $target_dir/$file_name
        wget $base_url/$file_name -P $target_dir
done

# get alternative config script
target_dir="config/dkrz/"
file_name="mistral.forpy.gcc"
wget $base_url/$file_name -P $target_dir
chmod +x $target_dir/$file_name
# modify config/buildbot/mistral_gcc
file_name="config/buildbot/mistral_gcc" 
sed -i 's/mistral.gcc/mistral.forpy.gcc/' $file_name



# back to the basic build script:
module unload gcc
module unload python
module unload python3
module load gcc/6.4.0
module load python/3.5.2
# as onliner:
# module unload gcc; module unload python; module unload python3; module load gcc/6.4.0; module load python/3.5.2
./config/dkrz/mistral.forpy.gcc
make
make_runscripts
```



well, we ar getting into interesting territory again:
```console
src/atm_phy_schemes/mo_2mom_mcrph_main.f90:887:105:

       x_r = rain%x_min ; CALL sedi_vel_rain(rain,rain_coeffs,q_r,x_r,rhocorr,vn_rain_min,vq_rain_min,1,1)
                                                                                                         1
Error: Type mismatch in argument 'its' at (1); passed REAL(8) to INTEGER(4)
src/atm_phy_schemes/mo_2mom_mcrph_main.f90:888:105:

       x_r = rain%x_max ; CALL sedi_vel_rain(rain,rain_coeffs,q_r,x_r,rhocorr,vn_rain_max,vq_rain_max,1,1)
                                                                                                         1
Error: Type mismatch in argument 'its' at (1); passed REAL(8) to INTEGER(4)
src/atm_phy_schemes/mo_2mom_mcrph_main.f90:895:109:

       x_r = rain%x_min ; CALL sedi_vel_rain(rain,rain_coeffs,q_r,x_r,rhocorr,vn_rain_min,vq_rain_min,1,1,q_c)
                                                                                                             1
Error: Type mismatch in argument 'its' at (1); passed REAL(8) to INTEGER(4)
src/atm_phy_schemes/mo_2mom_mcrph_main.f90:896:109:

       x_r = rain%x_max ; CALL sedi_vel_rain(rain,rain_coeffs,q_r,x_r,rhocorr,vn_rain_max,vq_rain_max,1,1,q_c)
                                                                                                             1
Error: Type mismatch in argument 'its' at (1); passed REAL(8) to INTEGER(4)
make[1]: *** [src/atm_phy_schemes/mo_2mom_mcrph_main.o] Error 1
make[1]: Leaving directory `/mnt/lustre01/work/mh0926/m300872/git/basic_icon_again'
make: *** [all] Error 2
```




### comparison
- [ ] compare multiple solutions
  - [ ] how easy are they?
  - [ ] how much interface code is needed, e.g. passing complex data types.
  - [ ] which datatypes are supported
  - [ ] how is the performance?
- [ ] test
  - [ ] write test case
  - [ ] try with all candidates
