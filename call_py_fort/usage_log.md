## installation

The [documentation](https://github.com/nbren12/call_py_fort) states the following dependendies:
- pfUnit (v3.2.9) for the unit tests
- python (3+) with numpy and cffi
- cmake (>=3.4+)

This should do the trick for cmake and python: `module load python cmake`
```console
m300872@mlogin104% python --version
Python 3.5.2
m300872@mlogin104% cmake --version
cmake version 3.17.1
```

numpy and cffi are there. I checked via some function calls in the python console
```python
>>> import numpy as np
>>> np.asarray([1,2,3])
array([1, 2, 3])
>>> from cffi import FFI
>>> FFI().new("int *")
<cdata 'int *' owning 4 bytes>
```
I installed pfUnit in the work directory with this script from the [_call_py_for_ documentation](https://github.com/nbren12/call_py_fort) 
```bash
# module load gcc/9.1.0-gcc-7.1.0
export F90=gfortran
export F90_VENDOR=GNU
export PFUNIT=/work/mh0926/m300872/pfunit

cd $PFUNIT
curl -L https://github.com/Goddard-Fortran-Ecosystem/pFUnit/archive/3.2.9.tar.gz | tar xz
cd pFUnit-3.2.9 
cmake .
make
make install INSTALL_DIR=${PFUNIT}
```
The usage apparently relies upon a runtime variable:
```bash
m300872@mlogin104% make install INSTALL_DIR=${PFUNIT}
[...]
+++
PFUNIT has been installed in /work/mh0926/m300872/pfunit.
For normal usage please ensure PFUNIT is set to /work/mh0926/m300872/pfunit.
For example, in bash: export PFUNIT=/work/mh0926/m300872/pfunit
```

Then we install the actual library
```bash
git clone https://github.com/nbren12/call_py_fort
cd call_py_fort
mkdir build
cd build 
cmake ..
# here some errors occur....
make
make install
```
the error is the following:
```console
CMake Warning at CMakeLists.txt:29 (find_package):
  By not providing "FindPFUNIT.cmake" in CMAKE_MODULE_PATH this project has
  asked CMake to find a package configuration file provided by "PFUNIT", but
  CMake did not find one.

  Could not find a package configuration file provided by "PFUNIT" with any
  of the following names:

    PFUNITConfig.cmake
    pfunit-config.cmake

  Add the installation prefix of "PFUNIT" to CMAKE_PREFIX_PATH or set
  "PFUNIT_DIR" to a directory containing one of the above files.  If "PFUNIT"
  provides a separate development package or SDK, be sure it has been
  installed.
````
Neither of these exist in my pfUnit install. but there is a `pfUnitConfig.cmake`, so i created a symbolic link with `ln -s pFUnitConfig.cmake PFUNITConfig.cmake` and did add `PFUNIT_DIR=/work/mh0926/m300872/pfunit/pFUnit-3.2.9` to the install script. This seems to be OK. I might have changed my cmake version somewhere in this process. Im currently using `cmake/3.11.4`

`make` now seemed to work, `make install` complained about not being able to write into `/usr/local`. But he shouldnt anyway. I want it installed in my work directory. I found out (grep magic), that this is defined in `build/CMakeCache.txt`, so i went and changed it. Its now installed in `/work/mh0926/m300872/callpy`:
```console
m300872@mlogin104% make install
[100%] Built target callpy
Install the project...
-- Install configuration: ""
-- Installing: /work/mh0926/m300872/callpy/lib/libcallpy.so
-- Set runtime path of "/work/mh0926/m300872/callpy/lib/libcallpy.so" to ""
-- Installing: /work/mh0926/m300872/callpy/include
-- Installing: /work/mh0926/m300872/callpy/include/callpy_mod.mod
-- Installing: /work/mh0926/m300872/callpy/lib/cmake/CallPyFort/CallPyFortConfig.cmake
-- Installing: /work/mh0926/m300872/callpy/lib/cmake/CallPyFort/CallPyFortConfig-noconfig.cmake
```
I have to think about linking in the future, though.



I might be repeating myself, but here is a install script

```bash
module purge
module load cmake python gcc                                                                  
                                                   
export PFUNIT=/work/mh0926/m300872/pfunitxport F90=gfortran
export FC=gfortran
export F90_VENDOR=GNU                                                                         

git clone https://github.com/nbren12/call_py_fort
# this modified callpy.py is not working as intended
# the issue im trying to solve is, that loading custom modules that are in the same directory does not work out of the box
cp callpy.syspathadded.py call_py_fort/src/callpy.py
cd call_py_fort
mkdir build
# here im commenting out the `add_pfunit_ctest`, because it was causing troubles.
# certainly not the best solution. it works for now.
cd buildcp ../../test-CmakeLists.testremoved.txt test/CMakeLists.txt
cmake ..  
make 
# `make install` will now try to install into `/usr/local`, which is not mistral conform
# The install directory (`CMAKE_INSTALL_PREFIX`) can be changed on line 136 of `build/CMakeCache.txt`
# This should be automated in the future (e.g. with sed)
make install
```


## woking with the examples

I wrote up a compile script:
```bash
#!/bin/bash

module load gcc
module load python

FC=gfortan
CALLPY=/work/mh0926/m300872/callpy
TARGET=test.f90

gfortran $TARGET -I $CALLPY/include -L $CALLPY/lib -lcallpy -Wl,-rpath -Wl,$CALLPY/lib:$(python3-config --prefix)/lib
```
I tried to convert it into a makefile, but somehow (???) the makefile does not translate $(python3-config --prefix) into an acutal path. IDK why.
```makefile
SHELL:=/bin/bash

CC=gfortran

# path to the callpy install directory
CALLPY="/work/mh0926/m300872/callpy"
PYTHON_PREFIX=${python3-config --prefix}
#PYTHON_PREFIX=/sw/rhel6-x64/python/python-3.5.2-gcc49

# Compiler flags:
FLAGS =  -Wextra -O2
# flags for debugging:
FLAGS =  -Wextra -Wall -g


a.out: 
#	echo "111 $(PYTHON_PREFIX)"; $(python3-config --prefix); \
#	echo "this should print the python base path: \n`python3-config --prefix`, \nand the python version: \n$(python --version), and <which python> $(which python)"
	$(CC) test.f90 -I $(CALLPY)/include -L $(CALLPY)/lib -lcallpy -Wl,-rpath -Wl,$(CALLPY)/lib -Wl,$(PYTHON_PREFIX)/lib $(FLAGS)

run: a.out
	./a.out

clean:
	rm -f *.mod *.o a.out
```

## Compile with ICON build system
Ralfs suggestion was, to just throw the `*.f90` files somewhere in the ICON `/src` directory and let the ICON build system deal with it. The Problem with this approach (which worked fine for *forpy*) is, that *call_py_fort*s shared object is created by *CFFI*, inside a python script with the command `ffibuilder.embedding_api(header)` (in `builder.py`).

## Compile as external library
To use it as an external library you just have to put it somewhere and tell the ICON config script, where to find it. The downside is, that you need precompiled versions for every compiler. It would be more convenient to have the build system build it for me.

Building the library worked without problems, using `gfortan` (using the modules: `python/3.5.2`, `cmake/3.17.1-gcc-9.1.0`, `gcc/6.4.0`). To use the intel compiler, i had to manipulate the `CMakeLists.txt`. I changed the `set(CMAKE_Fortran_COMPILER gfortran)` to `set(CMAKE_Fortran_COMPILER ifort)` and removed the flags `-warn errors warn all` (resulting in: `set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -g -traceback")`.

## Does not find functions.


i am trying to find out, why i can not load custom modules even though they are in the `PYTHONPATH`.

a few remarks on using functions:
- return values will not be printed. if the function does not manipulate a parameter in-place, calling it will be pointless.
- libraries are imported using `importlib.import_module(module_name)` on each function call
- call_py_fort passes the entire `STATE` to the function. This makes it desirable (in many cases required) to use `*args` in the function definition.
- call_py_fort only handles `real` type arrays with 1, 2 or 3 dimensions.
- existing functions (eg. `builtins`) that can not handle the input (see the two prior points) can not be called directly with call_py_forts `call_function`
