### what it is

It is also based on CFFI. It promises fast, no copy mechanisms to pass data into python. But this might have the same problem as _forpy_, as it relies on the same functionality (CFFI). The function calls seem intuitive to me (maybe just more C like). 

### getting the code

The [official documentation](https://forcallpy.readthedocs.io/en/latest/) offers an outdated git link to the repository. The project is located on a french academia git, called [_sourcesup_](https://sourcesup.renater.fr). Searching on _sourcesup_ yields a [minimalistic project site](https://sourcesup.renater.fr/scm/?group_id=3559) with a working git URL.


```bash
git clone https://git.renater.fr/anonscm/git/forcallpy/forcallpy.git
```
