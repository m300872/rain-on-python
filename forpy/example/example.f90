! short program to show the capabilities of Forpy to interconnect Fortran with Python
program forpy_example
  use forpy_mod
  use iso_fortran_env, only: real64
  implicit none

  integer :: ierror ! receives errorcodes

  ierror = forpy_initialize()

  write(*,*) ""
  write(*,*) ""
  write(*,*) "This is a short example of calling Python from Fortran."
  write(*,*) "  It uses the Forpy library (https://github.com/ylikx/forpy)"
  write(*,*) "  The program hands arrays and strings to Python and receives"
  write(*,*) "  return values in form of a dictionary that is then unpacked"
  write(*,*) "  and displayed."
  write(*,*) ""
  write(*,*) "The program will create two arrays (coefficient, ordinate)"
  write(*,*) "  containing random numbers. It then calls the python function"
  write(*,*) '  "solve" with a string containing an instruction.'
  write(*,*) "  Python uses numpys linear solver and returns the solution."
  write(*,*) "  To showcase Forpys ability to test for the existance of data,"
  write(*,*) "  Python with a 50% chance also sends an array containg numbers"
  write(*,*) "  in the returned dictionary."
  write(*,*) ""
  write(*,*) ""


  call dict_send_dict_receive()
  call forpy_finalize

  CONTAINS

  subroutine dict_send_dict_receive()
    ! module
    type(module_py) :: mymodule
    type(list) :: paths
    ! error handling
    integer :: ierror, e
    ! communicate with python
    type(dict)  :: kwargs, receive_dict, maybe_dict
    type(tuple) :: args, input_data
    type(object):: receive_object, receive_item
    type(tuple) :: receive_tuple
    real(kind=real64), dimension(:,:), pointer :: receive_matrix
    ! the matrices
    double precision, dimension(:,:), allocatable :: coefficient, ordinate, solution, optional_array, optional_array_dimensions
    type(ndarray) :: ord, cof, sol, opt, opt_dim
    ! loop indices
    integer :: i, j
    ! array dimensions
    integer :: ncol, nrow

    !load the module
    ierror = get_sys_path(paths)
    ierror = paths%append(".")
    ierror = import_py(mymodule, "module")

    ! create matrices
    allocate(coefficient(3,3))
    allocate(ordinate(3,1))
    allocate(solution(3,1))
    call random_number(coefficient)
    call random_number(ordinate)
    e = ndarray_create(cof, coefficient)
    e = ndarray_create(ord, ordinate)
    e = ndarray_create_empty(sol, [3,1], dtype="float64")

    ! ndarrays can also be created directly, e.g.:
    ! ierror = ndarray_create_empty(array, [NROWS, NCOLS], dtype="float64")

    ! the function call will need a tuple with arguments
    ! let it have an empty one, if not needed
    e = tuple_create(args,0)
    ! python equivalent of creating an empty dictionary 'kwargs = {}"
    e = dict_create(kwargs)
    e = kwargs%setitem("coefficent", cof)
    e = kwargs%setitem("ordinate", ord)
    e = kwargs%setitem("instruction", "pass")
    e = kwargs%setitem("instruction", "solve")

    ! print to stdout, to see, whats being set to python
    write(*,*) "fortran: print coefficient"
    do i = 1, 3
       write(*,*)  coefficient(i,:)
    end do
    write(*,*) ""
    write(*,*) "fortran: print ordinante"
    do i = 1, 3
       write(*,*)  ordinate(i,:)
    end do

    ! call of the actual python module
    e = call_py(receive_object, mymodule, "dict_solver", args, kwargs)

    ! unpack the received data
    e = dict_create(receive_dict)
    e = cast(receive_dict, receive_object)
    e = receive_dict%getitem(receive_object, "solution")
    e = cast(sol, receive_object)
    e = sol%get_data(receive_matrix)
    solution = receive_matrix

    ! print to stdout, to see what python calculated
    write(*,*) ""
    write(*,*) "fortran: print solution"
    do i = 1, 3
       write(*,*)  solution(i,:)
    end do
    write(*,*) ""

    ! testing the presence of an entry named "maybe_array" in the received data
    e = receive_dict%getitem(receive_object, "optional_array")
    if (e == 0) then
      write(*,*) "fortran: optional array found"
      e = cast(maybe_dict, receive_object)
      e = maybe_dict%get(ncol, "ncols", 1)
      e = maybe_dict%get(nrow, "nrows", 1)
      write(*,*) " dimension: (", ncol, ", ", nrow, ")"
      allocate(optional_array(ncol,nrow))
      e = ndarray_create_empty(opt, [nrow,ncol], dtype="float64")
      e = maybe_dict%getitem(receive_object, "data")
      e = cast(opt, receive_object)
      e = opt%get_data(receive_matrix)
      optional_array = receive_matrix

      ! and printing it to stdout
      write(*,*) ""
      write(*,*) "fortran: print optional_array"
      do i = 1, (nrow)
         write(*,*)  optional_array(i,:)
      end do
      write(*,*) ""
    else
      ! write(*,*) "no optional array."
      call err_clear
    endif

    ! brake stuff
    call args%destroy
    call kwargs%destroy
    deallocate(coefficient)
    deallocate(ordinate)
    call receive_tuple%destroy
    call receive_object%destroy
    call receive_item%destroy
    call mymodule%destroy
    call paths%destroy
  end subroutine

end program

