# File: mymodule.py

import numpy as np
import random


def dict_solver(*args, **kwargs):
    print("\npython: solver")
    print("\tinstruction given: ", kwargs["instruction"], "\n")
    if kwargs["instruction"] == "solve":
        kwargs["solution"]  = np.linalg.solve(kwargs["coefficent"], kwargs["ordinate"])
    elif kwargs["instruction"] == "pass":
        # we better give back a solutino anyway. its expected.
        kwargs["solution"] = np.zeros(kwargs["ordinate"].shape, dtype=np.float64, order='C')
    else:
        print("unknown instruction; exiting")
        return 1
    # we now (maybe) add an array with random numbers to test how
    # fortran handles this
    if bool(random.getrandbits(1)): # 50/50 chance of happening
        kwargs["maybe"] = True
        nrow = random.randint(1,6)
        ncol = random.randint(1,6)
        array = np.random.rand(ncol,nrow).transpose()
        optional_array = {"ncols":ncol, "nrows": nrow, "data":array}
        kwargs["optional_array"] = optional_array
    return kwargs

def check(*args, **kwargs):
    def check_tuple_or_dict(*x):
        print("\ttype(x) :", type(x))
        try:
            print("\tlen(x)  :", len(x))
            print("\tcontains:")
            for i in x:
                 print("\t\ttype(element):", type(i))
                 print("\t\tcontent :")
                 print("\t\t",i)
                 print(" ")
        # this was meant for input values, that have no length (e.g. integers)
        # probably not needed
        except AttributeError:
            print("\tcontent :")
            print("\t",x)
            print(" ")
    print("python: check")
    print("\tnon keyworded input arguments:")
    check_tuple_or_dict(args)
    print("\tkeyworded input arguments:")
    check_tuple_or_dict(kwargs)
    return 1

