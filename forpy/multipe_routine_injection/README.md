I talked to David Leutwyler again, to discuss how to expand the use of python. Until now the python module was called from inside the `accretionSB` subroutine (in `src/atm_phy_schemes/mo_2mom_mcrph_processes.f90`), replacing the fortran code on the lowest level. In this second step we move up a level and replace the calls for three subroutines of the warm rain processes in `src/atm_phy_schemes/mo_2mom_mcrph_main.f90:722-724` (autoconversionSB, accretionSB, rain_selfcollectionSB). 

Instead of manipulating the python function to accept single arrays instead of a object, containing those arrays, a python wrapper code will receive the data from fortran, create objects and pass them to the unaltered python module. 

The fortran subroutines use the arguments `ik_slice` (`INTEGER` array with length 4), dt (`REAL(wp)`), `cloud_coeffs` (`TYPE(particle_cloud_coeffs)`, a struct type class containing 6 `REAL(wp)`) and `cloud` and `rain` (both `TYPE(PARTICLE)`, a struct type class containing 13 `REAL(wp)`, 3 2D-arrays of `REAL(wp)` and a `CHARACTER(20)` name).

Both in the fortran and the python implementation, objects of the same class are used to represent clouds and rain. The fortran class is called `particle`, the python class is called `TwoMomentParticle`. Both are not classical OOP objects with private methods, but rather structs that bundle variables and constants. While `TwoMomentParticle` has a constructor, the `particle` is initialized by the `mo_2mom_mcrph_prosseses.f90` subroutine `particle_assign`. As these objects do not have other methods, they can be represented as a dictionary. Thus a dictionary is used to pass the data between fortran and python. 

There is a mismatch in properties of the `particle` class, defined in `src/atm_phy_schemes/mo_2mom_mcrph_types.f90` and properties of the corresponding `TwoMomentCloudParticle`, defined in `kinematic_rain_model/two_momment_microphysics.py`. 


| parameter | fortran | python | type | comment |
| --------- | ------- | ------ | ---- | ------- |
| name | x | - | `CHARACTER(20)` | value is `cloud_nue1mue1` in two moment scheme |
| q | x | x | 2D `REAL(wp)` array | mass density |
| N | x | x | 2D `REAL(wp)` array | number density |
| rho_v | x | x | 2D `REAL(wp)` array | density correction of terminal fall velocity |
| nx, ny, nz, nb | - | x | `int` | derived from `config`, used to initialize `q` and `N` |
| nu, mu | x | - |  `REAL(wp)` |  shape parameter of size distribution |
| x_max, x_min | x | x | `REAL(wp)` | min, max mean particle mass |
| a_geo, b_geo | x | x | `REAL(wp)` | pre-factor and exponent in diameter-mass relation |
| a_vel, b_vel | x | - | `REAL(wp)` | pre-factor and exponent in power law fall speed |
| a_ven, b_ven | x | - | `REAL(wp)` | parameter in ventilation coefficient |
| cap | x | - | `REAL(wp)` | coefficient for capacity of particle |
| vsedi_max, vsedi_min | x | - | `REAL(wp)` | min, max bulk sedimentation velocity |
| qcrit | - | x | `REAL(wp)` | set as `PARAMETER` in fortran (see below) |
| D_br | - | x | `REAL(wp)` | set as `PARAMETER` in fortran (see below) |

two of the parameter, that are set in the TwoMomentParticle.__init__() as a property of the particle are defined as `PARAMETER` in the original fortran code. `q_crit` is defined globally in `mo_2mo_mcrph_main.f90`, `D_br` is set as a local `PARAMETER` in two different subroutines inside `mo_2mo_mcrph_processes.f90`:
```fortran
36   : ! Re-write for ICON 04/2014 by AS:
37   : ! Some general notes:
44   : ! - Increased q_crit from 1e-9 to 1e-7 for efficiency. Looks ok for WK-test,
45:  : !   but has to be tested in a real-case setup with stratiform and cirrus clouds.
242  : REAL(wp), PARAMETER :: &
250  : !!!  &    q_crit    = 1.000e-7_wp, & ! q-threshold elsewhere 1e-7 kg/m3 = 1e-4 g/m3 = 0.1 mg/m3
251  :      &    q_crit    = 1.000e-9_wp, & ! q-threshold elsewhere 1e-7 kg/m3 = 1e-4 g/m3 = 0.1 mg/m3
1094 : SUBROUTINE rain_selfcollectionSB(ik_slice, dt, rain):
1111 : REAL(wp), PARAMETER :: D_br = 1.10e-3_wp
1314 : SUBROUTINE rain_evaporation(ik_slice, dt, rain_coeffs, rain_gfak, atmo, cloud, rain)
1341 : REAL(wp), PARAMETER :: D_br = 1.1e-3_wp
```
