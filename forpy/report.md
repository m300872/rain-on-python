# Forpy

The readme on Forpy's ![github repository](https://github.com/ylikx/forpy) offers a fairly detailed Guide with many code snippets of working example programs as a good starting point and reference. Further there is an ![API reference](https://ylikx.github.io/forpy/index.html) and a ![Wiki](https://github.com/ylikx/forpy/wiki) (The latter does not add much information).

## Compilation

On Mistral, I used the modules `gcc/9.1.0-gcc-7.1.0` and `python3/2021.01-gcc-9.1.0`. Compilation did not work right away, because the compiler would not find `libpython3.8.so.1.0`. Exporting the path to pythons libraries by hand solved the problem (`export LD_LIBRARY_PATH=/sw/spack-rhel6/miniforge3-4.9.2-3-Linux-x86_64-pwdbqi/lib:$LD_LIBRARY_PATH
`; the same solution with a less static code is used in the ![makefile of the provided example](https://gitlab.dkrz.de/m300872/rain-on-python/-/blob/master/forpy/example/makefile)). Note, that compilation with ![Python 2](https://github.com/ylikx/forpy#python-2-support) or ![Anaconda](https://github.com/ylikx/forpy#using-forpy-with-anaconda) is slightly different.

## Features

### Calling Python functions from Fortran code

Most of Forpys methods have to be used as `ierror = fuction(args)` with ierror being an integer that receives an error code. The integer receiving an error code is necessary. A python function can be called with (`call_py`) or without (`call_py_noret`) expecting a return value. Both function _optionally_ receive both a tuple `args` with arguments, and a dictionary `kwargs` with keyworded arguments. Data can **only**  be passed to python as a tuple or dictionary. Anything else will result in the the error `Error: There is no specific function for the generic 'call_py_noret' at (1)`. To pass e.g. a single integer to Python it has to be packed in a tuple.

It seemed to me, that if `kwargs` was used, `args` also had to be used (but could be empty). According to the ![API](https://ylikx.github.io/forpy/interface/call_py.html) `call_py` has a private function to deal with a call containing only `kwargs`.

Python functions without a defined return value will return `None`, which then will cause a problem when called with `call_py`. `call_py_noret` on the other hand ignores whatever value a python function returns and thus can always be used.

Python errors in functions, called by `call_py_noret` just result in a stop of the executed function, with no feedback. this does not crash the program and it is still possible to  make another function call as long as it does not produce a _Segmentation fault_. Calling non existant python functions with `call_py_noret` will give no feedback at all (and not produce a runtime error). Python errors in functions, called by `call_py` result in a _Segmentation fault_ during runtime. 

### Import of as modules

Non-standard python libraries can be used via the `import_py` function
```fortran
ierror = import_py(mymodule, "mymodule_filename")
ierror = call_py(return_value, mymodule, "function_name", args, kwargs)
```
Python errors never occur during compilation, not even if the referenced python library is not a valid file (e.g. a `.jpeg`). It thus makes sense to check the python module with `python mymodule_filename.py` before loading it into Fortran. 


### Use of complex data structures using lists, tuples, dictionaries

It is possible to both send (![necessary in that case](#calling-python-functions-from-fortran-code))) and receive tuples to and from python functions. The process is somewhat tedious as tuples have to be received as generic `object`s, then cast to a tuple (`cast(object, tuple)`), then dissected via `tuple%getitem(item,index)` and then each piece has again to be casted to a fortran data type. This also means, that the dimensions of the returned tuple have to be known. 

For the Python function to receive arguments they have to be packed as a tuple, that matches the number of parameters expected by the function. Alternatively the python function may contain a variadic input parameter (e.g. `def function(*args)`). Setting out of bound tuple elements with forpy will give no error message.


### Multidimensional arrays

Forpy provides two to forms of passing arrays to Python. Both are handled as `numpy.ndarray` on the Python side. An array is either created from a Fortran array (`ndarray_create`) or empty or filled with `0`s or `1`s (`ndarray_create_empty`, `ndarray_create_ones`, `ndarray_create_zeros`) and handed to Python as a copy. Alternatively arrays can be handed to Python as pointers via `ndarray_create_nocopy`. This way Python can manipulate the array directly. Note that rearranging array elements in Python may not transfer to its Fortran representation, only changing the values of array elements (e.g. inversing row order with `array = array[::-1]` leaves the Fortran array unchanged). Passing an array as a pointer also eliminates the need for type casting of the received data. 

Arrays in Fortran are represented as row-major, while in Python they are represented as column-major. Normally Forpy takes care of the proper conversion. Arrays that are created in Python and passed to Fortran (as opposed to array received and sent back) will be transposed (The ![provided example](https://gitlab.dkrz.de/m300872/rain-on-python/-/blob/master/forpy/example/module.py) transposes them before they are returned to Fortran to counter this issue). 


### Unpacking and Typecasting

As mentioned before, unpacking data receive from Python can be tedious. This is how a tuple containing a single array is unpacked in Fortran:
```fortran
ierror = call_py(receive_object, mymodule, "example_arrays", args, kwargs)
! unpacking:
ierror = cast(receive_tuple, receive_object)         ! cast nonspecific object to tuple            
ierror = receive_tuple%getitem(receive_item, 0)      ! get item  out of tuple        
ierror = cast(receive_array, receive_item)           ! cast unspecific object(receive_item) to array               
! It took me some time to notice, that this only works, if the array shape is set AFTER the cast()  ¯\_(ツ)_/¯
ierror = ndarray_create_empty(receive_array, [3,3], dtype="float64")
! this works. strange  enough. calling ndarray_create_ones() or ndarray_create_zeros() will overwrite the received data  though!
ierror = receive_array%get_data(receive_matrix)      ! now this will pass the data corretly
```

Forpy provides a few methods for to check the type of a received object (see ![API](https://ylikx.github.io/forpy/lists/procedures.html)). These can be used to to receive unknown objects from python (as shown ![here](https://github.com/ylikx/forpy/blob/master/README.md#tuples-objects)). Exact knowledge of the handled data types is useful and it might be wise to use explicit typecasting in Python (e.g. `a = a.astype(np.float64)`) to ensure handing the right data types back to Fortran. Python may do unexpected implicit conversions. In the ![provided example](https://gitlab.dkrz.de/m300872/rain-on-python/-/blob/master/forpy/example/example.f90) the presence of a dictionary entry ("optional_array") is tested and its content unpacked and displayed, if the dictionary entry is present. It further contains a data matrix of variable dimensions. The dimensions of this matrix are provided by the Python function and used to allocate a matrix in Fortran to fit the received data in.

## Example code

A ![example](https://gitlab.dkrz.de/m300872/rain-on-python/-/tree/master/forpy/example) of how data is passed to and received from a Python function is provided in `example/`, consisting of a python module, a fortran program and a makefile. 


## Known issues
[Multiple finalize/initialilze](https://github.com/ylikx/forpy/issues/10): Initializing and finalizing the forpy environment repeatedly does cause a crash. The problem here seems to lie in some C based libraries, e.g. numpy. The author recommends to call initialilze and finalize just once on a higher level, instead of inside a subroutine that is invoked repeatedly. My current workaround has been to not call `forpy_finalize` at all. This maybe should be adressed on the long run.

[Array storage order](https://github.com/ylikx/forpy/issues/31): Forpy does not check the storage order of arrays. This caused a problem in a single scenario, where an array was created in Python and then passed to Fortran. It probably is of little relevance to our use cases, as arrays that are created in Fortran, then passed forward and back are not affected. Array storage order can be specified to comply with Fortran when creating an array with numpy or it can be [specified during conversion](https://github.com/ylikx/forpy/issues/28).

# Under Construction: 

## Injection of Forpy into ICON

To compile ICON, a configure script is needed. Slight adjustmend have been made to the gcc configure script to include the compiler flags necessary for Forpy.
```bash
MODULES='gcc/6.4.0 openmpi/2.0.2p1_hpcx-gcc64 python/3.5.2'
[...]
PYTHON='/sw/spack-rhel6/miniforge3-4.9.2-3-Linux-x86_64-pwdbqi/bin/python'
[...]
BUILD_ENV="[...]:\${python3-config --prefix}/lib:\${LD_LIBRARY_PATH}\";"
[...]
LDFLAGS="[...] $(python3-config --ldflags) -Wl,-rpath -Wl,$(python3-config --prefix)/lib"
```
To properly resolve e.g. `${python3-config --prefix}` the same python module as specified in the config script should be loaded, when executing it.


The main problem here is that ICON, together with all its dependencies are build with gcc 6.4, while Forpy uses features introduced in later gcc versions to handle pointer arrays. An array can be handed between Fortran and Python, either as a copy or as a pointer. While handing copies is more secure, it can be costly with large arrays. When handing an array as a pointer, compiler optimization and MPI can mess with the arrays, e.g. returning outdated values. To prevent this either compiler optimization should be turned off (`-O0`) or the `asynchronous` or `volatile` keywords should be used in the array declaration on the Fortran side (see ![Forpy Documentation](https://github.com/ylikx/forpy/wiki/Working-with-arrays#compiler-optimization-related-bugs) and ![this Issue](https://github.com/ylikx/forpy/issues/3)). These keywords are only fully supported in gcc in versions >9.x (see ![release note](https://gcc.gnu.org/gcc-9/changes.html)). ICON, together with all its dependencies are build with gcc 6.4. Due to the nature of Fortran, changing the compiler for ICON would also require the change of the compiler for its dependenies.

### turning off compiler optimization
i tried to to that by replacing `-O3` and `-O2` in the config script with `-O0`. This produces errors during compilation (See ![error log](https://gitlab.dkrz.de/m300872/rain-on-python/-/blob/master/forpy/turn_off_compiler_optimization.errlog), that contains the last lines of the compiler output to the terminal)

### not use pointer arrows
instead of working on the array directly (subroutine-like behaviour) arrays can be handed to the pyhthon method as a copy and received back from python as a return value (more function-like behaviour). This approach does not need `asynchronous` arrays, but is probably much slower. This approach does compile and run (until given time limit). Neither the validity of the results, nor the performance has been assesed.

The implementation of this approach has been commited, see:
```bash 
git show 0fe54e32f6823c5d4bdc6a2230468d51533b296d
```

Now 
- [ ] run the `short_atm_amip` experiment with Forpy (`icon-cimd)
- [ ] run the `short_atm_amip` experiment without Forpy (e.g. `icon00`)
- [ ] compare the output
- [ ] does `run/exp.atm_amip.run` use two moments microphysics?
