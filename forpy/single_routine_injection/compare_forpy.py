mport numpy as np
import os, sys
import matplotlib.pyplot as plt
from PIL import Image
from datetime import datetime


# run directory set to ./ at the moment
csv_dir = "experiments/ruy0012/"
csv_dir = "./"
timestamp = str(datetime.now().hour) + str(datetime.now().minute) + str(datetime.now().second)
h = str(datetime.now().hour)
m = str(datetime.now().minute)
s = str(datetime.now().second)
for i in [h,m,s]:
	if len(i) < 2: #fill a leading zero
		i = "0" + i
target_dir = "png_" + h + m + s + "/"
os.mkdir(target_dir)
print("\n  saving plots in " + target_dir + "\n")

# make list of CSV in working directory
reduced_list = []
for i in os.listdir(csv_dir):
	if i.find(".csv") != -1:   # only CSV
		reduced_list.append(i)
reduced_list.sort()
logfile = csv_dir + reduced_list[-1]
log_number = reduced_list[-1][0:4]

# parse list and get list of pids, timestamps, array names and
pids  = []
tims = []
tabs = []
for i in reduced_list:
	split = i.split("_")
	pids.append(split[0])
	tims.append(split[1])
	tabs.append(split[2])
# remove duplicates
pids = list(dict.fromkeys(pids)) # process IDs
tims = list(dict.fromkeys(tims)) # timestamps
tabs = list(dict.fromkeys(tabs)) # array names
stts = ['pyt', 'for']            # pre or post python accretion method
# because the reduced_list is already sorted, these are, too

for a in [0,1,2]:              # iteration over variables, i.e. [Cloud q, Cloud N, Rain q]
	for p in range(len(pids)): # iteration over Process IDs
		# create the plot
		fig, axs = plt.subplots(nrows=4, ncols=3, gridspec_kw={'hspace': 0.2, 'wspace': 0.2})
		
		for t in [0,1,2,3]:
			filename = csv_dir + pids[p] + '_' + tims[t] + '_' + tabs[a] + '_' + stts[1] + '.csv'
			data_f = np.genfromtxt(filename, delimiter=',')
			filename = csv_dir + pids[p] + '_' + tims[t] + '_' + tabs[a] + '_' + stts[0] + '.csv'
			data_p = np.genfromtxt(filename, delimiter=',')
			diff = np.absolute(data_p - data_f)
			# set scale
			max_value = max(np.amax(data_f), np.amax(data_f))
			if max_value == 0:
				max_value = 1.0
			scale = {'max': max_value, 'min': 0.0}
			
			# draw the plots
			axs[t,0].imshow(data_f, 
				interpolation='none',
				vmin=scale['min'], vmax=scale['max']
				)
			axs[t,1].imshow(data_p,
				interpolation='none',
				vmin=scale['min'], vmax=scale['max']
				)
			axs[t,2].imshow(diff,
				interpolation='none',
				vmin=scale['min'], vmax=scale['max']
				)
			# attach the relative and absolut maximum value of diff as text (using the x axis label)
			rel_max_diff = max(0, np.amax(diff) / max(np.amax(data_f),np.amax(data_p))) # maximum of diff as fraction of max value in both data_f and data_p. max(0, [...]) to catch nan values
			axs[t,2].set_xlabel("max\nabs: " + "{0:.3g}".format(np.amax(diff)) + "\nrel: " + "{0:.3g}".format(rel_max_diff))
		
		# label rows and columns
		cols = ['fortran', 'python', 'diff']
		rows = ['t1 ', 't2 ', 't3 ', 't4 ']
		for ax, col in zip(axs[0], cols):
			ax.set_title(col)
		for ax, row in zip(axs[:,0], rows):
			ax.set_ylabel(row, rotation=0, size='large')
			fig.suptitle('PID ' + pids[p] + ', ' + tabs[a] , fontsize=16)
		
		# remove ticks
		for ax in axs:
			for axx in ax:
				axx.set_xticks([])
				axx.set_yticks([])

		fig.subplots_adjust(hspace=0.05)
		fig.savefig(target_dir + pids[p] + '_' + tabs[a] + '.png')
		#plt.show()
		plt.close(fig)
