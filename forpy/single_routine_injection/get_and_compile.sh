#!/bin/bash

# automatically installs icon and mods it with a single python routine
# April 2021, hernan.campos@mpimet.mpg.de


base_directory_name="icon_forpy"
repository="cimd"
forpy_enabled=true


# loading modules, needed during compilation
module unload gcc
module unload python
module unload python3
module unload python2
module load gcc/6.4.0 
module load openmpi/2.0.2p1_hpcx-gcc64 
module load python/3.5.2 


# translate the repository short name
case "$repository" in
"base")
	icon_repository="git@gitlab.dkrz.de:icon/icon.git"
	;;
"aes")
	icon_repository="git@gitlab.dkrz.de:icon/icon-aes.git"
	;;
"cherry")
	icon_repository="--branch icon-aes-two_cherry git@gitlab.dkrz.de:icon/icon-aes.git"
	;;
*)
	icon_repository="git@gitlab.dkrz.de:icon/icon-cimd.git"
	;;
esac


# get the repo
git clone --recursive $icon_repository $base_directory_name
cd $base_directory_name

if [ $repository == "aes" ]; then
	git checkout icon-aes-two
	wget https://gitlab.dkrz.de/m300872/rain-on-python/-/raw/master/forpy/single_routine_injection/work/mh0287/m214002/GIT/icon-aes-two_new/run/exp.atm_2mom_bubble_rceTorus -P run
fi

if [ $forpy_enabled ]; then
	# get the files
	target_dir="src/atm_phy_schemes/"
	#base_url="https://gitlab.dkrz.de/m300872/rain-on-python/-/blob/master/forpy/single_routine_injection/"
	base_url="https://gitlab.dkrz.de/m300872/rain-on-python/-/raw/master/forpy/single_routine_injection/"
	file_list=(
			"mo_2mom_mcrph_processes.f90"
			"two_moment_microphysics.py"
			"forpy_mod.f90"
	)
	# loop over list and wget them
	for file_name in "${file_list[@]}"; do
			echo "getting: $value"
			rm $target_dir/$file_name
			wget $base_url/$file_name -P $target_dir
	done


	# get alternative config script
	target_dir="config/dkrz/"
	file_name="mistral.forpy.gcc"
	wget $base_url/$file_name -P $target_dir
	chmod +x $target_dir/$file_name
	# modify config/buildbot/mistral_gcc
	file_name="config/buildbot/mistral_gcc" 
	sed -i 's/mistral.gcc/mistral.forpy.gcc/' $file_name

	./config/dkrz/mistral.forpy.gcc
else
	./config/dkry/mistral.gcc
fi


# back to the basic build script:
make -j8

if [ $repository == "aes" ]; then
	./make_runscripts  -r run -s atm_2mom_bubble_rceTorus
else
	./make_runscripts
fi
