# dependencies of the oringinal module
from numpy import square, sqrt, cbrt, log, exp, tanh  # Transcendental Functions
from numpy import maximum, minimum, logical_and, logical_not, arange

from numpy import pi as np_pi
from numpy import float32

# Added by H.Campos
import numpy as np
from datetime import datetime

class config:
	"""
	# Principal parameters
	# The essential control parameters according to Seifert and Steves (2010)
	"""

	# Data Types
	# -------------------------------------------------
	dtype = float32

	tau_w = dtype(50 * 60)  # Cloud Life Time
	w00 = dtype(2.0)  # Maximum Updraft Speed [m/s]
	lapseRate = dtype(3.75)  # In-Cloud Lapse Rate [K / km]. Eyeballed from paper.
	N_a = dtype(100 * 1e6)  # Ambient CCN [m-3]

	# Print options
	# -------------------------------------------------
	idbg = False  # print debugging text (False = not print, True = print)

	# Model Configuration
	# -------------------------------------------------
	dt = dtype(1.0)  # Timestep [s] #DL: That is a very small timestep: CFL will be  close to 0
	tend = 50 * 60  # integration time [s]

	z_max = dtype(5000.0)  # Domain height [m]
	nz = 240  # Number of vertical levels
	dz = dtype(round(z_max / nz, 0))  # spacing between vertical layers [K]

	nx = 1  # Number of grid columns in lon direction
	ny = 1  # Number of grid columns in lat direction
	nb = 1  # number of boundary points

	# Output control
	# -------------------------------------------------
	out_fname = "output"  # file name of output
	iout = 1  # write every iout-th time-step into the output file

	# Initial Conditions
	# -------------------------------------------------
	w0 = w00  # Vertical Wind [m/s]
	z0 = dtype(500)  # Cloud base [m]
	rh_cb = dtype(1.0)  # Relative humidity at cloud base, zero above
	p_cb = dtype(950.0e2)  # Pressure at cloud base

	# Boundary Conditions
	# -------------------------------------------------
	p0 = dtype(1000.0e2)  # Surface Pressure [Pa]
	theta0 = dtype(297.2)  # Potential temperature at the surface [K]


class const:
	"""
	Physical constants
	"""

	pi = config.dtype(np_pi)
	rho0 = config.dtype(1.065)  # reference air density for shallow clouds [kg m-3.] #ICON has 1.225
	g = config.dtype(9.80665)  # av. gravitational acceleration [m/s2]

	rd = config.dtype(287.04)  # Gas constant dry air [J/K/kg]
	rv = config.dtype(461.51)  # Gas constant for water vapor [J/K/kg]

	cpd = config.dtype(1004.64)  # Specific heat of dry air at constant pressure [J/K/kg]

	tmelt = config.dtype(273.15)  # Melting temperature of water [K]
	dv0 = config.dtype(2.22e-5)  # Diffusion coefficient of water vapour in air at tmelt [m^2/s]
	rhow = config.dtype(1000.0)  # Density of liquid water [kg/m3]




# def mean_particle_mass(q, N, x_min, m_max):
def mean_particle_mass(q, N):
	"""
	Obtain the mean particle mass from the distribution
	"""
	# eps is a small number to protect from division by 0
	# const.eps:   eps = config.dtype(1e-25)  # A small number:   dtype = float32
	eps = float(1e-25)
	# x_min and x_max are particle specific, i.e. different for rain and cloud. 
	# They are thus initilized at the object creation. However, we only use the clouds properties. 
	# These are given in simulations.py as: 
	x_max=3.00e-06
	x_min=2.60e-10
	return minimum(maximum(q / (N + eps), x_min), x_max )


#def accretion(dt, CloudWater_q, Cloudwater_N, Rain_q):
def accretion(dt, CloudWater_q, CloudWater_N, Rain_q, **kwargs): # pid added for debugging
#def accretion(dt, CloudWater_q, Cloudwater_N, Rain_q, *args, **kwargs): # pid added for debugging
#def accretion(*args, **kwargs): # pid added for debugging
	# Accretion of Seifert and Beheng (2001, Atmos. Res.)
	# ICON Routine: accretionSB
	if config.idbg:
		print("Accreation")
		#const.eps
		#self.dt = ?
		#TwoMomentMicrophysics.mean_particle_mass
	
	timestamp = str(datetime.now().hour) + str(datetime.now().minute) + str(datetime.now().second)
	h = str(datetime.now().hour)
	m = str(datetime.now().minute)
	s = str(datetime.now().second)
	for i in [h,m,s]:
		if len(i) < 2: #fill a leading zero
			i = "0" + i
	timestamp = h + m + s
	
	write_dir = "/work/mh0926/m300872/git/cherry_gcc_05"
	exp_dir   = "/experiments/ruy0012"
	#outfile = open(write_dir+"/LOG_accretion.txt", "a+")
	#outfile.writelines(m)
	#outfile.close()	 
	#np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'rainQ' + '_' + 'pre.csv', Rain_q, delimiter=',')
	#np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'clouQ' + '_' + 'pre.csv', CloudWater_q, delimiter=',')
	#np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'clouN' + '_' + 'pre.csv', CloudWater_N, delimiter=',')
	

	m = ""
	
	#m += "python accretion " + kwargs['pid'] + "\n"
	#m += "    " + str(args) + "\n"
	m += "pre:  " + "\n"
	m += "  dt: " + str(dt) + "\n"
	m += "  cq: " + str(CloudWater_q) + "\n"
	m += "  cn: " + str(CloudWater_N) + "\n"
	m += "  rn: " + str(Rain_q) + "\n"
	m += "      " + str(kwargs) + "\n"
	m += "  \n"
	#m += "\t" + str(Rain_q) + str(type(Rain_q)
	#write_dir="/work/mh0926/m300872/git/cherry_gcc_05"
	#outfile = open(write_dir+"/LOG_accretion.txt", "a+")
	#outfile.writelines(m)
	#outfile.close()	 
	#print(m)
	#return 2
	
	k_r = 5.78  # kernel
	k_1 = 5.0e-04  # Phi function
	eps = float(1e-25)

	tau = 1.0 - CloudWater_q / (CloudWater_q + Rain_q + eps)
	phi = (tau / (tau + k_1)) ** 4
	ac = k_r * CloudWater_q * Rain_q * phi * dt
	ac = minimum(CloudWater_q, ac)

	# Limit accreation to cloudy and rainy points
	ind = (CloudWater_q > 0.0) & (Rain_q > 0.0)

	Rain_q[ind] += ac[ind]
	CloudWater_q[ind] -= ac[ind]
	CloudWater_N[ind] -= minimum(CloudWater_N, ac / mean_particle_mass(CloudWater_q, CloudWater_N))[ind]
	
	return_dict = {
		'cloud_q': CloudWater_q,
		'cloud_n': CloudWater_N,
		'rain_q' : Rain_q
		}
	
	m += "post: " + "\n"
	m += "  dt: " + str(dt) + "\n"
	m += "  cq: " + str(CloudWater_q) + "\n"
	m += "  cn: " + str(CloudWater_N) + "\n"
	m += "  rn: " + str(Rain_q) + "\n"
	m += "      " + str(kwargs) + "\n"
	m += "  \n"
	#m += "\t" + str(Rain_q) + str(type(Rain_q)
	#write_dir = "/work/mh0926/m300872/git/cherry_gcc_05"
	#exp_dir   = "/experiments/ruy0012"
	#outfile = open(write_dir+"/LOG_accretion.txt", "a+")
	#outfile.writelines(m)
	#outfile.close()	 
	#np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'rainQ' + '_' + 'pos.csv', Rain_q, delimiter=',')
	#np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'clouQ' + '_' + 'pos.csv', CloudWater_q, delimiter=',')
	#np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'clouN' + '_' + 'pos.csv', CloudWater_N, delimiter=',')
	#print(m)
	
	return return_dict





# ########################################################
# The following functions were used for debugging purposes
# ########################################################

def save_csv_dual(dt, CloudWater_q, CloudWater_N, Rain_q, CloudWater_q_py, CloudWater_N_py, Rain_q_py, **kwargs): 
# this has the same variables as accretion to facilitate its usage in the context of accretion
# dt is not used here at all
	# make a timestamp
	timestamp = str(datetime.now().hour) + str(datetime.now().minute) + str(datetime.now().second)
	h = str(datetime.now().hour)
	m = str(datetime.now().minute)
	s = str(datetime.now().second)
	for i in [h,m,s]:
		if len(i) < 2: #fill a leading zero
			i = "0" + i
	timestamp = h + m + s
	#define where to store the file
	write_dir = "/work/mh0926/m300872/git/cherry_gcc_05"
	exp_dir   = "/experiments/ruy0012"
	# write
	np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'rainQ' + '_' + "for" + '.csv', Rain_q, delimiter=',')
	np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'clouQ' + '_' + "for" + '.csv', CloudWater_q, delimiter=',')
	np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'clouN' + '_' + "for" + '.csv', CloudWater_N, delimiter=',')
	np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'rainQ' + '_' + "pyt" + '.csv', Rain_q_py, delimiter=',')
	np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'clouQ' + '_' + "pyt" + '.csv', CloudWater_q_py, delimiter=',')
	np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'clouN' + '_' + "pyt" + '.csv', CloudWater_N_py, delimiter=',')
	#outfile = open(write_dir+"/LOG_accretion.txt", "a+")
	return 1

def save_csv(dt, CloudWater_q, CloudWater_N, Rain_q, **kwargs): 
# this has the same variables as accretion to facilitate its usage in the context of accretion
# dt is not used here at all
	# make a timestamp
	timestamp = str(datetime.now().hour) + str(datetime.now().minute) + str(datetime.now().second)
	h = str(datetime.now().hour)
	m = str(datetime.now().minute)
	s = str(datetime.now().second)
	for i in [h,m,s]:
		if len(i) < 2: #fill a leading zero
			i = "0" + i
	timestamp = h + m + s
	#define where to store the file
	write_dir = "/work/mh0926/m300872/git/cherry_gcc_05"
	exp_dir   = "/experiments/ruy0012"
	# write
	np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'rainQ' + '_' + kwargs['method'] + '.csv', Rain_q, delimiter=',')
	np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'clouQ' + '_' + kwargs['method'] + '.csv', CloudWater_q, delimiter=',')
	np.savetxt(write_dir + exp_dir + '/'  + str(kwargs['pid']) + '_' + timestamp + '_' + 'clouN' + '_' + kwargs['method'] + '.csv', CloudWater_N, delimiter=',')
	#outfile = open(write_dir+"/LOG_accretion.txt", "a+")
	return 1

def do_nothing():
	#message = "python two_mom_module: doing nothing\n"
	#write_dir="/work/mh0926/m300872/git/cherry_gcc_05"
	#outfile = open(write_dir+"/LOG_nothing.txt", "a+")
	#outfile.writelines(message)
	#outfile.close()	 
	#print(message) #, file=sys.stdout)
	return 1

def plot(arr1, arr2=0, pid=111111):

#import itertools
#import numpy as np
#from numpy import array
	import matplotlib.pyplot as plt
#import random
	fig, ax = plt.subplots()
	#fig = ax.figure
	#pixel = ax.imshow(arr1, interpolation='nearest', cmap='hot')
	pixel = ax.imshow(arr1, cmap='viridis')
	fig.colorbar(pixel)
	fig.savefig(str(pid) + "plot.png")





def check(*args, **kwargs):
	# This has become ugly over time. Im afraid of restructuring it, so ill just comment abundantly
	#print("two_mom_module: check")

	# This is the god function, It gets called twice in the main body
	def check_tuple_or_dict(*x): 
		m = ""
		# level 0
		# we only expect a tuple or dict (args or kwargs), so not much info is needed
		try: # iteration will only work on list, dict, tuple, etc.
			m += "\tcontains:\n" 
			for i in x:
				# level 1
				#m += "level 1 " + str(type(i)) 
				# now we wanna now everything
				m += "\t\ttype(element):" + str(type(i)) + "\n"
				m += "\t\tlen(element) :" + str( len(i)) + "\n"
				m += "\t\tcontent :" + "\n"
				if isinstance(i, np.ndarray):
					m += "\t\t\tarray of :"  + str(i.dype) + "\n"
					m += "\t\t\tdimension:"  + str(i.ndim) + "\n"
				elif isinstance(i, str):
					m += "\t\t\tstring\n"
					m += "\t\t\tlength : "  + str(len(i)) + "\n"
					m += "\t\t\tcontent: "  + str(i) + "\n"
				elif isinstance(i, dict):
					for key in i:
						j = i[key]
						m += "\t\t\tkey(element) :" + key + "\n"
						m += "\t\t\ttype(element):" + str(type(j)) + "\n"
						if hasattr(j, '__len__'):
							m += "\t\t\tlen(element) :" + str( len(j)) + "\n"
						m += "\t\t\tcontent :" + "\n"
						m += "\t\t\t\t" + str(j) + "\n"
						if isinstance(j, np.ndarray):
							m += "\t\t\t\tarray of :"  + str(j.dtype) + "\n"
							m += "\t\t\t\tdimension:"  + str(j.ndim) + ", size: " + str(j.size) + "\n"
						m += " " + "\n"
				elif isinstance(i, (list, tuple)): #dict, tuple)):
					for j in i:
						# level 2
						#m += "level 1 " + str(type(i)) + "level 2 " + str(type(j))
						m += "\t\t\ttype(element):" + str(type(j)) + "\n"
						if hasattr(j, '__len__'):
							m += "\t\t\tlen(element) :" + str( len(j)) + "\n"
						m += "\t\t\tcontent :" + "\n"
						if isinstance(j, np.ndarray):
							m += "\t\t\t\tarray of :"  + str(j.dtype) + "\n"
							m += "\t\t\t\tdimension:"  + str(j.ndim) + ", size: " + str(j.size) + "\n"
						m += "\t\t\t\t" + str(j) + "\n"
						m += " " + "\n"
				elif isinstance(j, np.ndarray):
					m += "\t\t\tarray of :"  + str(j.dtype) + ", size: " + str(j.size) + "\n"
					m += "\t\t\tdimension:"  + str(j.ndim) + ", size: " + str(j.size) + "\n"
				else:
					m += "\t\t\t" + str(i) + "\n"
					m += " " + "\n"

		except AttributeError: # this is meant for input values, that have no length (e.g. integers)
		# this may not even be needed, as we only work with args and kwargs
			if isinstance(j, np.ndarray):
				m += "\t\t\t\tarray of :"  + str(j.dtype) + "\n"
				m += "\t\t\t\tdimension:"  + str(j.ndim) + "\n"
			#else:
			m += "\t\t" + str(i) + "\n"
			m += " " + "\n"
		return m

	# main body
	m  = "python: check\n"
	m += "\tnon keyworded input arguments:\n"
	m += check_tuple_or_dict(args)
#	m += str(args) + "\n"
	m += "\tkeyworded input arguments:\n"
	m += check_tuple_or_dict(kwargs)
#	m += str(kwargs) + "\n"
	
	write_dir="/work/mh0926/m300872/git/cherry_gcc_05"
	outfile = open(write_dir+"/LOG_check.txt", "a+")
	outfile.writelines(m)
	outfile.close()	 
	print(m)	
	return 1


if __name__ == "__main__":
	# for testing purposes:
	g = [.1,.2,.3]
	gs = [g,g,g]
	arr  = np.asarray(gs)
	
	#print(check(args))
	#print(check(np.asarray(args), args))
	#print(check(np.asarray([args,args]), 187))
	#print(check(7, 8, {'a': 1, 'b': 2, 'c':3}, extra="kese"))

	#print(check(arr, arr, arr, arr, test=True))
	
	#plot(arr)
	accretion(0.2, arr, arr, arr, test=True, pid=00000)



