Turned out, I forgot to `call(forpy_initialize)`.

I produce a new error (`/work/mh0926/m300872/git/cherry_gcc_05/run/LOG.exp.ruy0012.run.29561779.o`):
```console
 echam_phy_main: compute forcing by two-moment bulk microphysics (two)
Exception ignored in: <module 'threading' from '/sw/rhel6-x64/python/python-3.5.2-gcc49/lib/python3.5/threading.py'>
Traceback (most recent call last):
  File "/sw/rhel6-x64/python/python-3.5.2-gcc49/lib/python3.5/threading.py", line 1283, in _shutdown
    assert tlock.locked()
SystemError: <built-in method locked of _thread.lock object at 0x7f9fe21fb800> returned a result with an error set
Exception ignored in: <module 'threading' from '/sw/rhel6-x64/python/python-3.5.2-gcc49/lib/python3.5/threading.py'>
Traceback (most recent call last):
  File "/sw/rhel6-x64/python/python-3.5.2-gcc49/lib/python3.5/threading.py", line 1283, in _shutdown
    assert tlock.locked()
```


I dont get it. every function call from a custom model, crashes everything... :sad:

CURRENTLY WORKING AT `cherry-gcc_05`

## repeated calling of forpy_initialize

I wrote a python script to help me debug. probably a combersome way to approach this problem. anyway. the problem seems to be rooted in the repeated call of `forpy_initialize`. 
```python
m300872@mlogin103% python paranal.py 29575330
['15888', 'init', 'forpy\n']
['15888', '0', 'init', 'forpy\n']
['15888', 'load', 'module\n']
['15888', '0', 'get', 'sys', 'path\n']
['15888', '0', 'append', '.\n']
['15888', '0', 'append', 'explicit\n']
['15888', 'create', 'arrays\n']
['15888', '0', 'ndcreate', 'n_cloud_q\n']
['15888', '0', 'ndcreate', 'n_cloud_n\n']
['15888', '0', 'ndcreate', 'n_rain_q\n']
['15888', '0', 'ndcreate', 'ik_slice\n']
['15888', 'pack', 'args\n']
['15888', '0', 'tuple', 'create', 'args\n']
['15888', '0', 'set', 'item', '0', 'ik_slice.\n']
['15888', '0', 'set', 'item', '1', 'cloud_q\n']
['15888', '0', 'set', 'item', '2', 'cloud_n\n']
['15888', '0', 'set', 'item', '3', 'rain_q\n']
['15888', 'print', 'out', 'date\n']
['15888', '0', 'import_py\n']
['15888', '0', 'datetime%getattribute(date,', 'date\n']
['15888', '0', 'call_py(today,', 'date,', 'today\n']
['15888', '0', 'cast', 'today_str\n']
['15888', 'Today', 'is', '2021-05-06\n']
['15888', 'init', 'forpy\n']
```

I tried to call `forpy_initialize` in ` src/parallel_infrastructure/mo_mpi.f90`, but i did not spend much time on that yet. maybe a valuable approach. maybe im just leaving my territory. maybe i shouldnt touch that. i have to check if that is even helping. 

### module path

The approach I used before to load the forpy module did not work anymore. loading the modules returned errors. This could be hotfixed by adding an absolute path to the code. This is **not** good!:
```fortran
err = get_sys_path(module_paths)
! not working:
! err = module_paths%append(".")
err = module_paths%append("/work/mh0926/m300872/git/cherry_gcc_05/src/atm_phy_schemes/")
err = import_py(py_module, "two_moment_microphysics")
```
Also i spend some time on finding the error that was importing the module as `import_py(py_module, "two_moment_microphysics.py"))` instead of `import_py(py_module, "two_moment_microphysics"))` (file extension)

### forpy_finalize

If I call forpy_finalize, I wont start up again. So the way, that works is, to call forpy_initialize, but never close the environment. At least this way no segmentation faults are produced and the run finishes. I think this is mentioned somewhere in the forpy documentation, but i could not find it.


## Toothless is working
showing our python teeth crashes the run. next try will be:
call the python routine, but dont use the output, only to compare it to the fortran output. in a first step
- [ ] compare type + dimensions
- [ ] then also content maybe
- [ ] also check if nothing is translated
- [ ] comparison inside the model run, report to file (or `stdout`, i.e. `run/LOG*.o`)

