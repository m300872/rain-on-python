### Usage log
a simple hello world compiles fine using the `gfortran` from the module `gcc/9.1.0-gcc-7.10`:
```console
m300872@mlogin108% cat hello.f90 
program hello
  ! This is a comment line, it is ignored by the compiler
  print *, 'Hello, World!'
end program hello
m300872@mlogin108% module load gcc/9.1.0-gcc-7.10
m300872@mlogin108% gfortran hello.f90 
m300872@mlogin108% ./a.out 
 Hello, World!
```

I tried to follow the README provided in the repository  (https://github.com/ylikx/forpy).
compilation on mistral seems to  work with modules `python3/2021.01-gcc-9.1.0` and `python/2.7.12`, but running the produced `a.out`  gives a missing library.

```console
m300872@mlogin108% gfortran -c forpy_mod.F90 
m300872@mlogin108% gfortran intro_to_forpy.f90 forpy_mod.o `python3-config --ldflags --embed` 
m300872@mlogin108% ./a.out 
./a.out: error while loading shared libraries: libpython3.8.so.1.0: cannot open shared object file: No such file or directory
```
export LD_LIBRARY_PATH=/sw/spack-rhel6/miniforge3-4.9.2-3-Linux-x86_64-pwdbqi/lib:$LD_LIBRARY_PATH


this is similar under python 2.7, although  compilation is slightly different (see forpy docs).

testing on a Ubuntu laptop was succesful:
```console
h@urz-trash:~/forpy_test$ gfortran --version
GNU Fortran (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0
Copyright (C) 2017 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
h@urz-trash:~/forpy_test$ python --version
Python 3.7.1
h@urz-trash:~/forpy_test$ ls
a.out  forpy_mod.f90  forpy_mod.F90  forpy_mod.mod  forpy_mod.o  simple.f90
h@urz-trash:~/forpy_test$ cat simple.f90 
program intro_to_forpy
  use forpy_mod
  implicit none

  integer :: ierror
  type(list) :: my_list

  ierror = forpy_initialize()
  ierror = list_create(my_list)

  ierror = my_list%append(19)
  ierror = my_list%append("Hello world!")
  ierror = my_list%append(3.14d0)
  ierror = print_py(my_list)

  call my_list%destroy
  call forpy_finalize

end program
h@urz-trash:~/forpy_test$ gfortran -cpp -c forpy_mod.f90 
h@urz-trash:~/forpy_test$ gfortran -cpp simple.f90 forpy_mod.o -fno-lto `python3-config --ldflags`
h@urz-trash:~/forpy_test$ ./a.out 
[19, 'Hello world!', 3.14]
```

The *missing library error* encountered on the mistral system can be solved by manually adding the python library path to the environment variable `LD_LIBRARY_PATH`. Spack unsets variables like LD_LIBRARY_PATH, LIBRARY_PATH and CPATH by default, because they may cause problems.  Spack has a *dirt* setting to not  clean up these environment variables ([spack documentation](https://spack.readthedocs.io/en/latest/module_file_support.html#)). This might cause problem on the long term. 


```console
m300872@mlogin108% gfortran -c forpy_mod.F90 
m300872@mlogin108% gfortran intro_to_forpy.f90 forpy_mod.o `python3-config --ldflags --embed` 
m300872@mlogin108% ./a.out 
./a.out: error while loading shared libraries: libpython3.8.so.1.0: cannot open shared object file: No such file or directory
m300872@mlogin100% which python
/sw/spack-rhel6/miniforge3-4.9.2-3-Linux-x86_64-pwdbqi/bin/python
m300872@mlogin108% export LD_LIBRARY_PATH=/sw/spack-rhel6/miniforge3-4.9.2-3-Linux-x86_64-pwdbqi/lib:$LD_LIBRARY_PATH
m300872@mlogin108% gfortran intro_to_forpy.f90 forpy_mod.o `python3-config --ldflags --embed` 
m300872@mlogin100% ./a.out 
[19, 'Hello world!', 3.14]
```

non standard python libraries can be used via the _import_py_ function:
```fortran
ierror = forpy_initialize()
! Instead of setting the environment variable PYTHONPATH,
! we can add the current directory "." to sys.path
ierror = get_sys_path(paths)
ierror = paths%append(".")
  
ierror = import_py(mymodule, "mymodule")
! then call the functions via call_py
ierror = call_py(return_value, mymodule, "print_args", args, kwargs)
ierror = cast(return_string, return_value)
write(*,*) return_string
! or via call_py_noret, if no return value is desired
ierror = call_py_noret(mymodule, "print_args")
```

creating numpy arrays is possible, as well as using a wrapper to access fortran arrays directly without copying. 

to pass an array to python it has to be converted, using _ndarray_create()_, then put in a tuple. to receive the tuple properly the corresponding python function should exactly match the number of items in the tuple or have a variadic input parameter (_def function(*args)). Setting out of bound tuple elements with forpy will give no error message. It will just not be there.

```fortran
type(ndarray) :: array
type(tuple) :: args
real, dimension(5) :: in_array
integer :: ii

do ii = 1,5
  in_array = real(ii)
enddo

ierror = ndarray_create(arr, in_array)
ierror = tuple_create(args,1)
ierror = args%setitem(0,arr)
! this will produce no error, but also have no effect:
ierror = args%setitem(1, "goes nowhere")

ierror = call_py_noret(mymodule,  "myfunction", args)
```


I dont get how to convert back to fortran, but maybe this is not even necessary, as the wrapper method may be the preferred method anyway.

feeding integers into  a python function, that had a _print(len(x))_ statement, made me realise the following:
 - python errors in functions, called by _call_py_noret_ just result in a stop of the executed function, with no feedback. this does not crash the program and it is still possible to  make another function call as long as it does not produce a _Segmentation fault_.
 - python errors in functions, called by _call_py_ result in a Segmentation fault during runtime
 - Errors never occur during compilation, not even if the referenced python library is not a valid file (e.g. a JPEG), it makes sense to check the python library beforehand (e.g. _python mymodule.py_)
 - calling non existant python functions with _call_py_noret_ will give no feedback at all (and give no runtime error)


with the _ndarray_create_nocopy_ method, an array can be handed to the python function without copying it at the transfer. This even eliminates the need to receive an array and to convert it back. the downside is safety. on the other hand it could just be copyied inside the python function, or before handing it to the python function. Not all changes python does to its internal representation of the array influence the fortran array. changing values affects the fortran array, changes in order are not necessarily transferred to fortran. e.g. a _array = array[::-1]_ (inverse row order) leaves the fortran array unchanged.
 - changed values are changed for fortran too
 - changes to the array order might not affect the fortran array


It is possible to both send (maybe even necessary in that case) and receive tuples to and from python functions. The process is tedious as tuples have to be received as _objects_, then cast (_cast_(from,to)_) to a tuple, then dissected via _tuple%getitem(item,index)_, then each piece has again to be casted to a fortran data type. This also means, that the dimensions of the tuple have to be known.


another source of error are calling function with _call_py()_ which expects a return value, but the python function does not provide one. as usual there is no error feedback. the run will just ommit that function call.

Receiving a tuple (probably also a list) you have to know the lenght of it to dissect it. Filling up to a standard length of e.g. 1000 using _None_ or _False_ and identifying the lenght of the list (i.e. data in the list without filler) in fortran may be an option.

Data can **only**  be passed to python as a tuple or dictionary. Anything else will result in the the following: 
```console
Error: There is no specific function for the generic 'call_py_noret' at (1)
```

The forpy methods have to be called using the form shown in the tutorial, i.e. with a integer value which will then receive an error code 
```console
m300872@mlogin101% fpcompile forpy_example.f90; ./a.out
forpy_example.f90:46:15:

  46 |     dict_create(kwargs)
     |               1
Error: 'dict_create' at (1) is not a variable
m300872@mlogin101% fpcompile forpy_example.f90; ./a.out
forpy_example.f90:3:6:
    3 |   use forpy_mod
......
   46 |     call dict_create(kwargs)
      |                            2
Error: 'dict_create' at (1) has a type, which is not consistent with the CALL at (2)
```

There is way to receive ndarrays from python and to cast them back into fortran data types. But it requires exact knowledge of the received data type. In my example i send data to python as _double precision_, but the returned array can only be read if real64 (_real(ind=real64)_) is used.

```fortran
double precision, dimension(:,:), allocatable :: ordinate
type(ndarray) :: ord
real(kind=real64), dimension(:,:), pointer :: matrix

allocate(ordinate(3,3))           ! create matrices
call random_number(ordinate)      ! fill with random floats
e = ndarray_create(ord, ordinate) ! convert to ndarray

e = ord%get_data(matrix)          ! get data back
```

While sending an array to python, letting pythno manipulate it and receiving it back is relatively easy, creating one in phython and then send it to Fortran is not as straight forward. Of course dataype and Dimensions have to be known. Emphasis here is on the data type! It has to be casted in Python. e.g:
```python
def example_arrays()
# returns a tuple with two example arrays
  a = np.array([[2,2,1],[3,4,1],[4,5,1]]) 
  b = np.array([[12, 19, 24]])
  a = a.astype(np.float64)
  b = b.astype(np.float64)
return (a,b)
```
This is how the tuple containing the array is unpacked in Fortran:
```fortran
e = call_py(receive_object, mymodule, "example_arrays", args, kwargs)
e = cast(receive_tuple, receive_object)         ! cast nonspecific object to tuple            
e = receive_tuple%getitem(receive_item, 0)      ! get item  out of tuple        
! too early to do this: e = ndarray_create_empty(receive_array, [3,3], dtype="float64")
e = cast(receive_array, receive_item)           ! cast unspecific object(receive_item) to array               
! the array shape has to be determined AFTER the cast()  ¯\_(ツ)_/¯
e = ndarray_create_empty(receive_array, [3,3], dtype="float64") ! (!)
! this works. strange  enough. calling ndarray_create_ones() or ndarray_create_zeros() will overwrite the received data  though!
e = receive_array%get_data(receive_matrix)      ! now this will produce  sensible output
```

sending and receiving i wrote a python function, that should just receive _ndarray_, and return it unchanged. This returns a transposed version of the original array. This is because Fortran arrays are stored in column-major order (_A(3,2) A(1,1) A(2,1) A(3,1) A(1,2) A(2,2) A(3,2) A(1,3) A(2,3) A(3,3)_), while Python (like C) arrays follow row-major order (_A[0][0] A[0][1] A[0][2] A[1][0] A[1][1] A[1][2] A[2][0] A[2][1] A[2][2]_). Therefore i tried tranposing the data before giving it back.
```python
def solver(*args, **kwargs):
    print("\npython: solver")
    print("\tinstruction given: ", kwargs["instruction"], "\n")
    a = kwargs["data"][0]
    b = kwargs["data"][1]
    print("\treceiving a = ", a )
    print("\treceiving b = ", b )
    print("\twith types: ", (a.dtype,b.dtype) )

[...]
    elif kwargs["instruction"] == "pass":
        pass
[...]

    # apparently we have to transpose the data before sending it back
    a = a.transpose()
    print(" ")
    print("\treturning a = ", a )
    print("\treturning b = ", b )
    print("\twith types: ", (a.dtype,b.dtype) )
    print("\n\n")
    return (a, b)
```
This results in correct representation of matrices sent to python and received back:
```console
m300872@mlogin103% ./a.out 
 fortran: print coefficient
  0.39611391307904931       0.38862832351581733       0.28556400639788049     
  0.95858147931422966       0.74031904322541386       0.92374943236775331     
   4.1984611956255247E-002  0.84991217994798118       0.41834791500166491     
 
 fortran: print ordinante
  0.99148715505060969     
  0.44855900526592141     
  0.14306259589793024     
 

python: solver
	instruction given:  pass 

	receiving a =  [[0.39611391 0.38862832 0.28556401]
 [0.95858148 0.74031904 0.92374943]
 [0.04198461 0.84991218 0.41834792]]
	receiving b =  [[0.99148716]
 [0.44855901]
 [0.1430626 ]]
	with types:  (dtype('float64'), dtype('float64'))
 
	returning a =  [[0.39611391 0.95858148 0.04198461]
 [0.38862832 0.74031904 0.84991218]
 [0.28556401 0.92374943 0.41834792]]
	returning b =  [[0.99148716]
 [0.44855901]
 [0.1430626 ]]
	with types:  (dtype('float64'), dtype('float64'))


 fortran: print coefficient
  0.39611391307904931       0.38862832351581733       0.28556400639788049     
  0.95858147931422966       0.74031904322541386       0.92374943236775331     
   4.1984611956255247E-002  0.84991217994798118       0.41834791500166491     
 
 fortran: print ordinante
  0.99148715505060969     
  0.44855900526592141     
  0.14306259589793024 
  ```
What is curious about it is that the ndarray is displayed in the correct order by python upon receiving. This might indicate, that the unwanted transpositon of the matrix is on its way out of Python.  But, creating a new matrix in Python and handing it back to  Fortran does not show this behaviour, i.e. it will be returned transposed with the new hotfix of transposing the data before sending it back to Fortran:
```console

m300872@mlogin103% ./a.out 
 fortran: print a
  0.36120009038635004       0.51917656852403193       0.52927073383357071     
  0.51201523038761299       0.38683417921225427       0.48750845348569805     
  0.12498476273834513       0.14422144075757659       0.42459724012034394     
 
 fortran: print b
  0.91991811866073181     
  0.62884232382075789     
  0.55258147327553020     

python: solver
	instruction given:  example 

	receiving a =  [[0.36120009 0.51917657 0.52927073]
 [0.51201523 0.38683418 0.48750845]
 [0.12498476 0.14422144 0.42459724]]
	receiving b =  [[0.91991812]
 [0.62884232]
 [0.55258147]]
	with types:  (dtype('float64'), dtype('float64'))
 
	returning a =  [[2. 3. 4.]
 [2. 4. 5.]
 [1. 1. 1.]]
	returning b =  [[12. 19. 24.]]
	with types:  (dtype('float64'), dtype('float64'))


 fortran: print a
   2.0000000000000000        2.0000000000000000        1.0000000000000000     
   3.0000000000000000        4.0000000000000000        1.0000000000000000     
   4.0000000000000000        5.0000000000000000        1.0000000000000000     
 
 fortran: print b
   12.000000000000000     
   19.000000000000000     
   24.000000000000000     
```

error codes that are returned by forpy allow to test for the existence of variable. In the following example the presence of a dictionary entry ("optional_array") is tested and its content unpacked and displayed, if the dictionary entry is present. It further contains a data matrix of variable dimensions. The dimensions of this matrix are provided by the Python function and used to allocate a matrix in Fortran to fit the received data in.


```fortran
    ! testing the presence of an entry named "optional_array" in the received data
    e = receive_dict%getitem(receive_object, "optional_array")
    if (e == 0) then
      write(*,*) "optional array found"
      e = cast(maybe_dict, receive_object)
      e = maybe_dict%get(ncol, "ncols", 1)
      e = maybe_dict%get(nrow, "nrows", 1)
      write(*,*) "(", ncol, ", ", nrow, ")"
      allocate(optional_array(ncol,nrow))
      e = ndarray_create_empty(opt, [nrow,ncol], dtype="float64")
      e = maybe_dict%getitem(receive_object, "data")
      e = cast(opt, receive_object)
      e = opt%get_data(receive_matrix)
      optional_array = receive_matrix

      write(*,*) ""
      write(*,*) "fortran: print optional_array"
      do i = 1, (nrow)
         write(*,*)  optional_array(i,:)
      end do
      write(*,*) ""

    else
 !     write(*,*) "no optional array."
      call err_clear
    endif
  ```


### compilation errors in icon base repository
Compiling the `git@gitlab.dkrz.de:icon/icon.git` gives some conversion errors (REAL to INT) in `src/atm_phy_schemes/mo_2mom_mcrph_main.f90`
This happens when I inject my source code, altough that file is untouched. It does not happen without my code injection. It also does not happen in `icon-cimd` nor does it in `icon-aes:two-cherry`

### min and max in mean_particle_mass
baustelle: `mean_particle_mass(CloudWater_q, CloudWater_N)` requires min and max as additional inputs (contained in the object in original) 
I got them from the `simulation.py` and put them into the function. They are originally particle dependent and part of the object (i.e. `cloud` and `rain`), but accretion only uses the `min` and `max` values of `cloud`. So i just hard coded these into `mean_particle_mass()`. Not perfect, but enough for accretion.

### single method injection works
The next step is time measurement. 

>**hernan.campos** 11:07 AM
>
>Optionen wären:
> - time report (hab ich das richtig verstanden, dass man im ICON code einfach marker setzen kann und die dann im LOG auftauchen?)
> - mehr fortran code durch python ersetzen (eventuell das ganze Modul einbauen)
> - den bisherigen code nochmal in einem weniger minimalistischen Setup testen.
>
>**rene.redler** 11:56 AM
>
>Ich würde das in der Reihenfolge 1.) timer 2.) weniger minimalistisch 3.) mehr Fortran ersetzen angehen 
>oder alternativ 1.) timer 2.) andere Varianten der Python-Fortran Interoperabilität angehen.
>
>**daniel.klocke** 12:13 PM
>
>Sehe ich genau so.... 1.) timer 2.) timer + weniger minimalistisch. 
>Wenn timer akzeptabel 3.) mehr Python, 
>wenn timer nicht akzetabel andere Python-Fortran Variante probieren.

### timer

so far i succeded in putting a timer in the `vim src/atm_phy_schemes/mo_2mom_mcrph_processes.f90`. But this lets appear one timer per process. Not very useful. So for now i failed putting the timer in `vim src/atm_phy_schemes/mo_2mom_mcrph_main.f90`, which is where `accretionSB()` is called. Timers rely on the the `mo_timer` methods `new_timer`, `timer_start` and `timer_end`. I modified `mo_2mom_mcrph_main.f90` in the following places:

 - added the new_timer method (line 141): `USE mo_timer, ONLY: new_timer, [...]` 
 - declare an integer to store the timer (line 537): `integer :: timer_accretion_py`
 - in the different (identical) stages of the else-if statement where accretion is called (line 682ff):
    - initilize the timer `timer_accretion_py = new_timer("2mom_accr")`
    - start the timer `call timer_start(timer_accretion_py)`
    - end the timer `call timer_end(timer_accretion_py)`
