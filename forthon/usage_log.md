[Forthon](https://github.com/dpgrote/Forthon) can be downloaded and installed with `python setup.py install` or via pip with `pip install Forthon`

The `INSTALL` text file states, that 
> Write permission is required for the python library site-packages directory.

I installed it via pip, and tried to ignore the write permission issue.
1. first error was `/bin/sh: Forthon3: command not found`, which i circumvented by replacing the `FORTHON = Forthon3` with the full path: `FORTHON = /work/mh0926/m300872/git/forthon/Forthon3`
2. error is about the write permissions in the library directory: `error: could not create '/sw/spack-rhel6/miniforge3-4.9.2-3-Linux-x86_64-pwdbqi/lib/python3.8/site-packages/forthon_example': Read-only file system`

maybe another python? how about anaconda?
```console
m300872@mlogin104% module purge
m300872@mlogin104% ls
Makefile  README  forthon_example_source  run_forthon_example.py
m300872@mlogin104% module li
No Modulefiles Currently Loaded.
m300872@mlogin104% module load anaconda3/bleeding_edge 
m300872@mlogin104% ls
Makefile  README  forthon_example_source  run_forthon_example.py
m300872@mlogin104% make
(cd forthon_example_source ; /work/mh0926/m300872/git/forthon/Forthon3 --install --pkgdir . forthon_example)
Traceback (most recent call last):
  File "/work/mh0926/m300872/git/forthon/Forthon3", line 2, in <module>
    import Forthon.Forthon_builder
ModuleNotFoundError: No module named 'Forthon'
make: *** [install] Error 1
m300872@mlogin104% cd ..
m300872@mlogin104% ls
Forthon3  INSTALL  License.txt  MANIFEST  README.md  Release_Notes  docs  example  example2  setup.py  simpleexample  source  update  version.py
m300872@mlogin104% python --version
Python 3.6.10 :: Anaconda, Inc.
m300872@mlogin104% python setup.py install
Traceback (most recent call last):
  File "setup.py", line 23, in <module>
    commithash = subprocess.check_output(['git', 'log', '-n', '1', '--pretty=%h'], stderr=subprocess.STDOUT, text=True).strip()
  File "/sw/rhel6-x64/conda/anaconda3-bleeding_edge/lib/python3.6/subprocess.py", line 356, in check_output
    **kwargs).stdout
  File "/sw/rhel6-x64/conda/anaconda3-bleeding_edge/lib/python3.6/subprocess.py", line 423, in run
    with Popen(*popenargs, **kwargs) as process:
TypeError: __init__() got an unexpected keyword argument 'text'
```
