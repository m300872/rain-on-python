#!/bin/bash
# hernan.campos@mpimet.mpg.de

# this scripts keeps track of the jobid, generates one based on a stored counter and writes changes in a log file

INITIALS=ruy
COUNTER_FILE=~/.jobid/counter.txt
LOG_FILE=~/.jobid/logfile.txt

# in case the files do not exist
touch $LOG_FILE
touch $COUNTER_FILE

# get the count as argument or from file
if [ -z "$1" ]; then
        # echo "NO INPUT"       
        COUNT=`cat $COUNTER_FILE`
        ((COUNT++))
        # save current count
        echo $COUNT > $COUNTER_FILE
else
        if ! [[ "$1" =~ ^[0-9]+$ ]]; then
                # echo "INPUT NOT INT"
                # treated like no input at all
                COUNT=`cat $COUNTER_FILE`
                ((COUNT++))
                # save current count
                echo $COUNT > $COUNTER_FILE
        else
                # echo "INPUT INT"
                COUNT=$1
        fi
        # read
fi

# final form
JOB_ID="$INITIALS$(printf "%04d" $COUNT)"
# log changes
echo $(date) >> $LOG_FILE
echo "$JOB_ID" >> $LOG_FILE
echo "" >> $LOG_FILE
# return value
echo $JOB_ID 
