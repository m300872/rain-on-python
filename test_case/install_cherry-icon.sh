#!/bin/bash 
# script installs the two-cherry branch of icon-aes
# the two moment microphysics test works with this branch

# SET VARIABLES HERE
exp_name=$(bash /pf/m/m300872/.jobid/generate_script_ID.sh) # uses script to generate a experiment ID
build_dir=cherry_gcc_04f # everthing goes here



# get the code
cd /work/mh0926/m300872/git/
git clone --recursive git@gitlab.dkrz.de:icon/icon-aes.git $build_dir
cd $build_dir 
git checkout icon-aes-two

# if you want to make changes to the code prior to compilation, now is a good time. uncomment:
#exit

# Configuration and Compilation
#./config/dkrz/mistral.intel-18.0.5 --enable-openmp  # this was recommended by Monica Esch
#./config/dkrz/mistral.forpy.gcc --enable-openmp  # this works too
./config/dkrz/mistral.gcc --enable-openmp  # this works
make -j 8

# get the run script template from Monika
twomom_script=atm_2mom_bubble_rceTorus
cp /work/mh0287/m214002/GIT/icon-aes-two_new/run/exp.$twomom_script run/exp.$exp_name
# or from the repo (same file):
#wget https://gitlab.dkrz.de/m300872/rain-on-python/-/raw/master/test_case/exp.atm_2mom_bubble_rceTorus 
#mv exp.atm_2mom_bubble_rceTorus run/exp.$exp_name

# generate run script:
./make_runscripts  -r run -s $exp_name
sed -i 's/account=mpiscl/account=mh0926/' run/exp.$exp_name.run
