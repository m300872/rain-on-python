I started using the example provided in the ICON quickstart tutorial (`run/exp.atm_amip.run`). Then I adjusted the `end_time` to make the model simulate only 12 hours (write intervals also had to be adjusted). This was helpful to test the proper compilation of the code, but I then realised, that two moment microphysics was not used in this test case at all. As a new starting point there is a test case from the AES group. It is contained in the repository ![icon-aes-two-cherry](https://gitlab.dkrz.de/icon/icon-aes/-/tree/icon-aes-two_cherry). 

> Schaut mal unter icon-aes:icon-aes-two_cherry, da hat zuletzt Sebastian einen buildbot-Test fürs 2mom (aus der Sapphirephysik heraus) eingebaut. Das ist vermutlich die einfachste Lösung - geht aber aktuell nur im icon-aes-two_cherry. Ich weiß ja nicht, in welchem branch ihr euch 'herumtreibt' - für die NWP Physik habe ich nichts parat.

I am not shure if i have the proper script, but `git log|grep 2mo|grep build` found me this: `./run/exp.atm_2mom_bubble_rceTorus_test `

```bash
m300872@mlogin108%  ./run/exp.atm_2mom_bubble_rceTorus_test 
./run/exp.atm_2mom_bubble_rceTorus_test: line 247: add_link_file: command not found
./run/exp.atm_2mom_bubble_rceTorus_test: line 248: add_link_file: command not found
./run/exp.atm_2mom_bubble_rceTorus_test: line 249: add_link_file: command not found
./run/exp.atm_2mom_bubble_rceTorus_test: line 253: add_required_file: command not found
cat: dict.iconam.mpim: No such file or directory
./run/exp.atm_2mom_bubble_rceTorus_test: line 259: add_required_file: command not found
./run/exp.atm_2mom_bubble_rceTorus_test: line 270: add_link_file: command not found
```
I might have just not compiled the code....


The ICON [`README.md`](https://gitlab.dkrz.de/icon/icon-aes/-/tree/icon-aes-two_cherry#faq-2) states that config scripts for buildbots are found under `config/buildbot/`. But no specific config script in mentioned in the run script and none of the existing config scripts has a apparent link to the run script. I will try `mistral_gcc`. Why do have buildbots their own config scripts, though?


Apparently the two moment microphysics are activated via a namelist, that is created by the run script via the switch `echam_phy_config(1)%l2moment  = .TRUE.`
```bash 
# (3) Define the model configuration

# atmospheric dynamics and physics
# --------------------------------
cat > ${atmo_namelist} << EOF
[...]
! surface (.TRUE. or .FALSE.)
 echam_phy_config(1)%ljsb   = .FALSE.
 echam_phy_config(1)%lamip  = .FALSE.
 echam_phy_config(1)%l2moment  = .TRUE. 
 echam_phy_config(1)%lice   = .FALSE.
 echam_phy_config(1)%lmlo   = .FALSE.
 echam_phy_config(1)%llake  = .FALSE.
!
```

I did compile and make it run like this:
```bash
./config/buildbot/mistral.gcc
make
make_runscripts
# changing the account name to mh0926
vim ./run/exp.atm_2mom_SP_bubble_torus.run
sbatch ./run/exp.atm_2mom_SP_bubble_torus.run
```

Now ill try migrate my code there.
```bash 
cd /work/mh0926/m300872/git # aliased as gowork in my .profile
git clone --recursive --branch icon-aes-two_cherry git@gitlab.dkrz.de:icon/icon-aes.git cherry3 # or any other directory name
cd cherry3
./config/buildbot/mistral_gcc
[...]
# TODO inject modified 2mo files
[...]
make
./make_runscripts
```

## Help from AES Monika:
>Erst einmal möchte ich dich bitten, den branch zu wechseln, damit du näher am aktuellen Code bist:
>Nutze bitte icon-aes:icon-aes-two
>
>Das ist der branch an welchem ich aktuell arbeite für das 2mom Schema, und der auch vollständig(er) mit dem
>Release-Kandidaten aktualisiert ist.
>Dort gibt es allerdings kein kurzes Testscript, daher bitte ich dich, folgendes dann in deine lokale Kopie zu holen:
>/work/mh0287/m214002/GIT/icon-aes-two_new/run/exp.atm_2mom_bubble_rceTorus (s.u.)
>
>Und dann fange, wenn möglich, bitte erst einmal mit dem Standardcompiler an.
>
>Wenn du folgende Kommandos 'abarbeitest', solltest du zumindest so weit kommen, dass das Modell läuft.
>Und bitte erst einmal mit dem unveränderten Code arbeiten - wenn der läuft, lädst du deine Änderungen dazu:
>
>cd /work/mh0926/m300872/git/
>git clone --recursive [mailto:git@gitlab.dkrz.de:icon/icon-aes.git]git@gitlab.dkrz.de:icon/icon-aes.git
>cd icon-aes-two
>git checkout icon-aes-two
>./config/dkrz/mistral.intel-18.0.5 --enable-openmp
>make -j 8
>
>Damit hast du das Modell ausgecheckt und compiliert (mit intel)
>Jetzt das Script ins run-directory kopieren:
>cp /work/mh0287/m214002/GIT/icon-aes-two_new/run/exp.atm_2mom_bubble_rceTorus run
>
>Dann das Script generieren:
>./make_runscripts  -r run -s atm_2mom_bubble_rceTorus
>
>(später solltest du deine eigenen Id's nutzen, siehe https://wiki.mpimet.mpg.de/doku.php?id=models:icon:running_the_model:experiment_developer_id)
>
>Dann im run-directory im gererierten Script 'exp.atm_2mom_bubble_rceTorus.run' deinen Account setzen und nodes=6 setzen und dann
>sbatch exp.atm_2mom_bubble_rceTorus.run
>
>Wenn das nicht läuft, melde dich bitte wieder, dann müssen wir weitersehen.
>
>Alles so ausführlich, weil du meintest du wärst noch ungelenk 😉.


```bash
cd /work/mh0926/m300872/git/
git clone --recursive git@gitlab.dkrz.de:icon/icon-aes.git icon-aes-two
cd icon-aes-two
git checkout icon-aes-two
./config/dkrz/mistral.intel-18.0.5 --enable-openmp
make -j 8

# Script ins run-directory kopieren:
cp /work/mh0287/m214002/GIT/icon-aes-two_new/run/exp.atm_2mom_bubble_rceTorus run

# Script generieren:
./make_runscripts  -r run -s atm_2mom_bubble_rceTorus
```

This runscript is not perfect, but it does the stuff it should. (WORK IN PROGRESS)

```bash
build_dir=icon-aes-two
build_dir=mon_cherry3

cd /work/mh0926/m300872/git/
git clone --recursive git@gitlab.dkrz.de:icon/icon-aes.git $build_dir
cd $build_dir 
git checkout icon-aes-two
./config/dkrz/mistral.intel-18.0.5 --enable-openmp
make -j 8

# Script ins run-directory kopieren:
2mom_script=exp.atm_2mom_bubble_rceTorus
# cp /work/mh0287/m214002/GIT/icon-aes-two_new/run/$2mom_script run
cp /work/mh0287/m214002/GIT/icon-aes-two_new/run/exp.atm_2mom_bubble_rceTorus run
#exp.atm_2mom_bubble_rceTorus run

# Script generieren:
# ./make_runscripts  -r run -s $2mom_script  
./make_runscripts  -r run -s atm_2mom_bubble_rceTorus
# sed -i 's/account=mpiscl/account=mh0926/' $2mom_script
sed -i 's/account=mpiscl/account=mh0926/' run/exp.atm_2mom_bubble_rceTorus.run
```

This error is my current problem:
```console
m300872@mlogin108% ./run/exp.atm_2mom_bubble_rceTorus.run 
+ ulimit -s unlimited
./run/exp.atm_2mom_bubble_rceTorus.run: line 41:  / 6 / 2: arithmetic syntax error
```

## plain two_cherry icon is working now
I dont know, what produced the above errors, but [the installation with this script](https://gitlab.dkrz.de/m300872/rain-on-python/-/blob/master/test_case/install_cherry-icon.sh) is working with both `config/dkrz/mistral.mpicc` and `config/dkrz/mistral.gcc`.

I wrote a [script to generate job ids](https://gitlab.dkrz.de/m300872/rain-on-python/-/blob/master/test_case/generate_jobID.sh) and created an alias for it with `alias generate_jobID='bash /pf/m/m300872/.jobid/generate_script_ID.sh'`

The two-cherry branch compiles and the `atm_2mom_bubble_rceTorus` test case runs just fine. It does that with configuration via `config/dkrz/mistral.mpicc --enable-openmp`, `config/dkrz/mistral.gcc --enable-openmp` and the modified version of the latter:  `config/dkrz/mistral.forpy.gcc --enable-openmp`. The test case requires openmp. 

## first try with python injection

I did compile the two_cherry branch with the injection of python code in the accretion. It did **not** work. The script ran longer than normal and produced segmentation faults. Longe than usual means that the test runs around 17s in the vanilla two-cherry branch (`/work/mh0926/m300872/git/cherry_gcc_03/run/LOG.exp.ruy0008.run.29550982.o`), and failed after 40s in the modified version (with python code; `/work/mh0926/m300872/git/cherry_gcc_04f/run/LOG.exp.ruy0010.run.29550956.o`)

I uploaded a [log of a failed run](https://gitlab.dkrz.de/m300872/rain-on-python/-/blob/master/test_case/LOG.exp.ruy0010.run.log). Ititialisation works normal, then at the first time step (`Time step:     1` [...]) it crashes. 

I asked for help in mattermost and this is what people found for me: 

```bash
mo_2mom_mcrph_util::init_dmin_wg_gr_ltab_equi: Table file dmin_wetgrowth_lookup.nc opened
mo_2mom_mcrph_util::init_dmin_wg_gr_ltab_equi: INFO: need table file for hydrometeor type graupelhail_cosmo5 but file dmin_wetgrowth_lookup.nc is for graupelhail2test4
```
deutet evt. darauf hin, dass ein Inputfile nicht so ist wie erwartet. 


Segmentation faults have the additional information `adress not mapped to object`


A few words on the model mechanics: 
>Nur zur Erklaerung: Das Model initialisiert und Macht in Time step 1 dann 4 Dynamische Substeps (5 waeren der Default??) . Danach werden die Parameterisierungen aufgerufen, die in jedem Time step aufgerufen werden. Erst die Strahlung (erst die Strahlungsflussberechnung, seperat fuer Long Wave und Short Wave, dann die Heizraten), dann die Turbulenz, oder verticale diffusion (vdf) und dann die Mikrophysik (two), wo es dann nicht weiter geht...

>Hm, da steht auch: `ndyn_substeps    = 5           ! dtime/dt_dyn`. Das kann dir aber egal sein, wird nichts mit dem Crash zu tun haben. Ein Substep fehlt, aber es kann sein, dass bei der Initialisierung was anders laeuft... wuerde ich mir jetzt keine Sorgen machen. 


In the runscript a `msg_level` defines the amount of ouput, that is logged. This is set to 12 in the `atm_2mom_bubble_rceTorus`, which is _quite high_ according to Daniel, but could be raised to e.g. 15. Crossing certain thresholds with the `msg_level` will produce more output. This will also make the run slower and thus should be tuned down for longer runs.

## Toothless injection

The first try of replacing accretion by a python method did not work. The next test will be more subtle: Injecting Python code, that maybe produces output, but does not interfere with the model run.

This is moved to the [forpy usage log](https://gitlab.dkrz.de/m300872/rain-on-python/-/blob/master/forpy/single_routine_injection/usage_log.md), because its forpy specific.

## python structure

It was decided (rather it was a suggestion by Daniel), that we would use the accretion function as a starting point. The python version of the kinematic rain module is not an exact copy of the fortran module. It makes use of object oriented concepts where they have not been used in the fortran code. The better 

```python
def accretion(self, CloudWater, Rain):
    # Accretion of Seifert and Beheng (2001, Atmos. Res.)
    # ICON Routine: accretionSB

    if config.idbg:
        print("Accreation")

    k_r = 5.78  # kernel
    k_1 = 5.0e-04  # Phi function

    tau = 1.0 - CloudWater.q / (CloudWater.q + Rain.q + const.eps)
    phi = (tau / (tau + k_1)) ** 4
    ac = k_r * CloudWater.q * Rain.q * phi * self.dt
    ac = minimum(CloudWater.q, ac)

    # Limit accreation to cloudy and rainy points
    ind = (CloudWater.q > 0.0) & (Rain.q > 0.0)            # an index list is created
    Rain.q[ind] += ac[ind]                                 # changes are applied only at positions contained in this list
    CloudWater.q[ind] -= ac[ind]
    CloudWater.N[ind] -= minimum(CloudWater.N, ac / TwoMomentMicrophysics.mean_particle_mass(CloudWater))[ind] 
    # dependency: def mean_particle_mass(TwoMomentCloudParticle):
```
used function
```python
def mean_particle_mass(TwoMomentCloudParticle):
    """
    Obtain the mean particle mass from the distribution
    """
    # eps is a small number to protect from division by 0
    return minimum(
        maximum(TwoMomentCloudParticle.q / (TwoMomentCloudParticle.N + const.eps), TwoMomentCloudParticle.x_min),
        TwoMomentCloudParticle.x_max,
    )
```
`x_min` and `x_max` are properties of the particle:
```python
class TwoMomentCloudParticle:
    def __init__(
[...]
        self.x_max = x_max  # maximal droplet mass
        self.x_min = x_min  # minimal droplet mass
```
These are set in `simulations.py`:
```python
self.CloudWater = TwoMomentCloudParticle(
    qcrit=1e-6,
    x_max=2.60e-10,
    x_min=4.20e-15,
    rho_vel=0.2,
)
self.Rain = TwoMomentCloudParticle(
    x_max=3.00e-06,
    x_min=2.60e-10,
    rho_vel=0.4,
    D_br=1.10e-3,
)
```





```fortran
SUBROUTINE accretionSB(ik_slice, dt, cloud,rain)
!*******************************************************************************
! Accretion of Seifert and Beheng (2001, Atmos. Res.)                          *
!*******************************************************************************
! start and end indices for 2D slices
! istart = slice(1), iend = slice(2), kstart = slice(3), kend = slice(4)
INTEGER,  INTENT(in) :: ik_slice(4)
REAL(wp), INTENT(in) :: dt
CLASS(particle), INTENT(inout)  :: cloud, rain
! start and end indices for 2D slices
INTEGER :: istart, iend, kstart, kend
INTEGER     :: i, k
REAL(wp)    :: ac
REAL(wp)    :: q_c, q_r, tau, phi, n_c, x_c

REAL(wp), PARAMETER :: k_r = 5.78_wp       ! kernel
REAL(wp), PARAMETER :: k_1 = 5.00e-04_wp   ! Phi function
REAL(wp), PARAMETER :: eps = 1.00e-25_wp

IF (isdebug) CALL message(routine, "accretionSB")

istart = ik_slice(1)
iend   = ik_slice(2)
kstart = ik_slice(3)
kend   = ik_slice(4)


DO k = kstart,kend                                        ! iterates over every point in the slice
   DO i = istart,iend                                     ! i.e. 2D slice
      n_c = cloud%n(i,k)
      q_c = cloud%q(i,k)
      q_r = rain%q(i,k)
      IF (q_c > 0.0_wp.AND.q_r > 0.0_wp) THEN             ! only applies accretion in cloudy of rainy areas

         !..accretion rate of SB2001
         tau = MIN(MAX(1.0-q_c/(q_c+q_r+eps),eps),1.0_wp)
         phi = (tau/(tau+k_1))**4
         ac  = k_r *  q_c * q_r * phi * dt

         ac = MIN(q_c,ac)

         x_c = particle_meanmass(cloud, q_c,n_c)

         rain%q(i,k)  = rain%q(i,k)  + ac
         cloud%q(i,k) = cloud%q(i,k) - ac
         cloud%n(i,k) = cloud%n(i,k) - MIN(n_c,ac/x_c)
      ENDIF:q
   END DO
END DO

END SUBROUTINE accretionSB:
```

I adapted the function `mean_particle_mass` to bring along whats needed:
```python
def mean_particle_mass(q, N):
    """
    Obtain the mean particle mass from the distribution
    """
    # eps is a small number to protect from division by 0
    # const.eps:   eps = config.dtype(1e-25)  # A small number:   dtype = float32
    eps = float(1e-25)
    # x_min and x_max are particle specific, i.e. different for rain and cloud. 
    # They are thus initilized at the object creation. However, we only use the clouds properties. 
    # These are given in simulations.py as: 
    x_max=3.00e-06
    x_min=2.60e-10
    return minimum(maximum(q / (N + eps), x_min), x_max )
```





### compare those things

Im at that step, where i compare values. Yay.
Thats where i left:
```bash
cat run/L*7435*|grep difference|less
```
