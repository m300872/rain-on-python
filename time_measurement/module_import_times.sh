import_time_report () {
        module=$1
        n=$2
        method=$3
        echo $module $n $3

        if [ -z "$3" ] 

        then # no input $3, i.e. specific function
                file_name="import_$module"
                echo '' > $file_name
                for (( i = 1; i <= $n; i++ )) ; do
                        python -X importtime -c "import $module" 2> tmp
                        tail -n 1 tmp >> $file_name # cumulative import time of the whole module is on the last line (bigger number)
                        rm tmp
                done

        else # input $3 given, i.e. import of specific function
                file_name="import_${module}_$3"
                echo '' > $file_name
                for (( i = 1; i <= $n; i++ )) ; do
                        python -X importtime -c "from $module import $3" 2> tmp
                        tail -n 1 tmp >> $file_name
                        rm tmp
                done
        fi
}

reps=10

#import_time_report "scipy" 5
import_time_report "numpy" $reps
import_time_report "numpy" $reps "minimum"
#import_time_report "matplotlib.pyplot" 5
#import_time_report "pandas" 5

# cumulative import time in micro-seconds (us) is the second integer on each line

# because it seemed like the simpler approach for the problem size, averages over the times where done in excel
